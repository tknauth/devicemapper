#!/usr/bin/env python2.7

import itertools
import glob
import os.path
import time
import shlex
import subprocess as sp

source_ip = '192.168.2.3'
target_ip = '192.168.2.2'

src_hdd = 'scsi-SATA_SAMSUNG_HD204UIS2H7J1CB117070-part4'
src_ssd = 'scsi-SATA_INTEL_SSDSC2CT1CVMP247001T8120BGN-part1'

tgt_hdd = 'scsi-SATA_SAMSUNG_HD204UIS2H7J1CB117088-part4'
tgt_ssd = 'scsi-SATA_INTEL_SSDSC2CT1CVMP244301DC120BGN-part1'

disk_id = {'src' : {'hdd' : src_hdd,
                    'ssd' : src_ssd},
           'tgt' : {'hdd' : tgt_hdd,
                    'ssd' : tgt_ssd}}

def get_src_file(disk) :
    assert disk == 'ssd' or disk == 'hdd'
    return '/dev/disk/by_id/'+disk_id['src'][disk]+'/source/f'

def get_tgt_file(disk) :
    assert disk == 'ssd' or disk == 'hdd'
    return '/dev/disk/by_id/'+disk_id['tgt'][disk]+'/source/f'

# (1) modify x% of blocks and (2) drop caches.

def sshrpc(cmd, ip) :
    ssh_cmd = 'ssh %s %s'%(ip, cmd)
    print ssh_cmd
    return sp.check_call(shlex.split(ssh_cmd))

def tgt(cmd) :
    return sshrpc(cmd, target_ip)

def src(cmd) :
    return sshrpc(cmd, source_ip)

class Experiment :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        self.size_gb = size_gb
        self.monitors = []
        self.run = run
        self.disk = disk
        self.src_file = '/mnt/%dGB.dat'%(self.size_gb)
        self.dst_file = '/mnt/%dGB.dat'%(self.size_gb)
        self.update_percentage = update_percentage
        self.transport = transport
        self.blocknr_file = blocknr_file
        self.tag = tag

    # @return String identifying the experiment, i.e., containing all
    # relevant parameters. Use, e.g., to name files.
    def expname(self) :
        tmp = '%s-%s-%s-%dGB-%s-%s-%02d'%(self.tag,
                                          self.__str__(),
                                          self.transport,
                                          self.size_gb,
                                          self.disk,
                                          os.path.basename(self.blocknr_file),
                                          self.run)
        return tmp
    
    def monitor(self, ip) :
        cmd = """rm /tmp/dstat ; dstat -tTcn -N eth0 --nocolor --noheaders --output /tmp/dstat >/dev/null 2>/dev/null &"""
        sp.call(['ssh', ip, cmd])

    def kill_monitor(self, ip) :
        cmd = 'pkill -SIGKILL -f %s'%('dstat')
        sp.call(['ssh', ip, cmd])
        # cannot have colons in file name with scp
        filename = '%s-%s-dstat'%(self.expname(), ip)
        filename = filename.replace(':','')
        sp.call(['scp', ip+':/tmp/dstat', filename])

    def setup_after_loopback_hook(self) :
        pass

    def teardown(self) :
        # Flush pending writes such that disassembling afterwards
        # does not fail.
        src('sudo sync')
        # disable_loopback(source_ip)

        tgt('sudo sync')
        # disable_loopback(target_ip)

        for ip in [source_ip, target_ip] :
            self.kill_monitor(ip)

    def setup_loopback(self) :
        src('sudo losetup /dev/loop0 %s'%(self.src_file))
        tgt('sudo losetup /dev/loop0 %s'%(self.dst_file))

    def teardown_loopback(self) :
        disable_loopback(source_ip)
        disable_loopback(target_ip)

    def setup_filesystem(self) :
        o = sp.check_output(shlex.split('ssh %s sudo file -s -L /dev/disk/by-id/%s' % (source_ip, disk_id['src'][self.disk])))
        if not 'ext4' in o :
            src('sudo mkfs.ext4 /dev/disk/by-id/%s' % (disk_id['src'][self.disk]))
            tgt('sudo mkfs.ext4 /dev/disk/by-id/%s' % (disk_id['tgt'][self.disk]))

        src('sudo mount /dev/disk/by-id/%s /mnt' % (disk_id['src'][self.disk]))
        tgt('sudo mount /dev/disk/by-id/%s /mnt' % (disk_id['tgt'][self.disk]))
        src('sudo chmod a+rw /mnt')
        tgt('sudo chmod a+rw /mnt')

        try :
            o = sp.check_output(shlex.split('ssh %s stat -c %%s /mnt/%dGB.dat' % (source_ip, self.size_gb) ))
        except sp.CalledProcessError, e :
            src('dd if=/dev/zero of=/mnt/%dGB.dat bs=1M count=%d'%(self.size_gb, self.size_gb*1024))
            tgt('dd if=/dev/zero of=/mnt/%dGB.dat bs=1M count=%d'%(self.size_gb, self.size_gb*1024))

    def update_blocks(self) :
        src('sudo ~/devicemapper/exp/update-random-blocks.sh %s %d'%(self.src_file, self.update_percentage))

    def setup(self) :
        self.update_blocks()
        src('echo 3 | sudo tee /proc/sys/vm/drop_caches')
        tgt('echo 3 | sudo tee /proc/sys/vm/drop_caches')

        self.monitor(source_ip)
        self.monitor(target_ip)

def disable_loopback(ip) :
    for i in range(5) :
        try :
            sshrpc('sudo losetup -d /dev/loop0', ip)
            break
        except sp.CalledProcessError, e :
            if i >= 5  : raise
            else : time.sleep(4**(i+1))

# Give path to a file with block numbers
# class PredeterminedBlocks :

#     def update_blocks(self) :
#         sp.check_call('cat %s | ssh %s sudo devicemapper/exp/update-blocks.sh %s'%(self.blocknr_file, source_ip, self.src_file), shell=True)

class RsyncExperiment(Experiment) :

    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        Experiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

        sync_cmd = {'sshnone' : 'rsync -e "ssh -oNoneSwitch=yes -oNoneEnabled=yes" --inplace --block-size=4096 %s %s:%s'%(self.src_file, target_ip, self.dst_file),
                    'sshhpn' : 'rsync --inplace --block-size=4096 %s %s:%s'%(self.src_file, target_ip, self.dst_file)}
        self.sync_cmd = sync_cmd[transport]

    def __str__(self) :
        return 'rsync'

    def setup(self) :
        Experiment.setup_filesystem(self)
        Experiment.setup(self)

    def teardown(self) :
        src('sudo umount /mnt')
        tgt('sudo umount /mnt')
        Experiment.teardown(self)

    def sync(self) :
        print self.sync_cmd
        sp.call(['ssh', source_ip, self.sync_cmd])
        tgt('sudo sync')

class CopyExperiment(Experiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        Experiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

        sync_cmd = {'sshhpn' : 'ssh %s scp %s %s:%s'%(source_ip, self.src_file, target_ip, self.dst_file),
                    'sshnone' : 'ssh %s scp -oNoneSwitch=yes -oNoneEnabled=yes %s %s:%s'%(source_ip, self.src_file, target_ip, self.dst_file)}
        self.sync_cmd = sync_cmd[transport]

    def __str__(self) :
        return 'copy'

    def setup(self) :
        Experiment.setup_filesystem(self)
        Experiment.setup(self)

    def teardown(self) :
        src('sudo umount /mnt')
        tgt('sudo umount /mnt')
        Experiment.teardown(self)

    def sync(self) :
        print self.sync_cmd
        sp.call(shlex.split(self.sync_cmd))
        tgt('sudo sync')

class DsyncExperiment(Experiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        Experiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

        sync_cmd = {'sshhpn' : 'sudo ~/devicemapper/code/dmextract.py loop0 | ssh %s sudo ~/devicemapper/code/dmmerge.py /dev/loop0'%(target_ip),
                    'sshnone' : 'sudo ~/devicemapper/code/dmextract.py loop0 | ssh -oNoneSwitch=yes -oNoneEnabled=yes %s sudo ~/devicemapper/code/dmmerge.py /dev/loop0'%(target_ip)}
        self.sync_cmd = sync_cmd[transport]

    def __str__(self) :
        return 'dsync'

    def setup(self) :
        Experiment.setup_filesystem(self)
        Experiment.setup_loopback(self)
        src('sudo modprobe dm-mod')
        src('echo 0 $(sudo blockdev --getsz /dev/loop0) linear /dev/loop0 0 | sudo dmsetup create loop0')
        self.src_file = '/dev/mapper/loop0'
        Experiment.setup(self)

    def teardown(self) :
        src('sudo dmsetup remove loop0')
        src('sudo rmmod dm-mod')
        Experiment.teardown_loopback(self)
        src('sudo umount /mnt')
        tgt('sudo umount /mnt')
        Experiment.teardown(self)

    def sync(self) :
        sshrpc(self.sync_cmd, source_ip)
        tgt('sudo sync')

class BlockMd5SyncExperiment(Experiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        Experiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

    def __str__(self) :
        return 'blockmd5sync'

    def setup(self) :
        Experiment.setup_filesystem(self)
        Experiment.setup(self)

    def teardown(self) :
        src('sudo umount /mnt')
        tgt('sudo umount /mnt')
        Experiment.teardown(self)

    def sync(self) :
        blocksync_opt = {'sshhpn' : '',
                         'sshnone' : '-e'}

        cmd = 'ssh %s blocksync.sh %s %s %s:%s'%(source_ip,
                                                 blocksync_opt[self.transport],
                                                 self.src_file,
                                                 target_ip,
                                                 self.dst_file)
        print cmd
        sp.call(shlex.split(cmd))
        tgt('sudo sync')

class ZfsExperiment(Experiment) :

    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        Experiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)
        
        self.src_file = '/source/f'
        self.dst_file = '/target/f'

        sync_cmd = {'sshhpn' : 'sudo zfs send -i source@1 source@2 | ssh %s sudo zfs receive target/snap'%(target_ip),
                    'sshnone' : 'sudo zfs send -i source@1 source@2 | ssh -oNoneSwitch=yes -oNoneEnabled=yes %s sudo zfs receive target/snap'%(target_ip)}
        self.sync_cmd = sync_cmd[transport]

    def __str__(self) :
        return 'zfs'

    def setup_after_loopback_hook(self) :
        # not required to set up any loopback devices for zfs send/recv
        pass

    def setup(self) :
        # src('sudo modprobe zfs')
        src('sudo zpool create -f source %s' % disk_id['src'][self.disk])
        tgt('sudo zpool create -f target %s' % disk_id['tgt'][self.disk])
        src('sudo dd if=/dev/zero of=/source/f bs=1M count=%d' % (self.size_gb*1024))
        src('sudo zfs snapshot source@1')
        src('sudo zfs send source@1 | ssh -oNoneSwitch=yes -oNoneEnabled=yes %s sudo zfs receive target/snap' % (target_ip))
        Experiment.setup(self)
        src('sudo zfs snapshot source@2')

    def teardown(self) :
        src('sudo zpool destroy source')
        tgt('sudo zpool destroy target')
        Experiment.teardown(self)

    def sync(self) :
        sshrpc(self.sync_cmd, source_ip)
        tgt('sudo sync')

class DsyncPre(DsyncExperiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        DsyncExperiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

    def update_blocks(self) :
        sp.check_call('cat %s | ssh %s sudo devicemapper/exp/update-blocks.sh %s'%(self.blocknr_file, source_ip, self.src_file), shell=True)

class RsyncPre(RsyncExperiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        RsyncExperiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

    def update_blocks(self) :
        sp.check_call('cat %s | ssh %s sudo devicemapper/exp/update-blocks.sh %s'%(self.blocknr_file, source_ip, self.src_file), shell=True)

class CopyPre(CopyExperiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        CopyExperiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

    def update_blocks(self) :
        sp.check_call('cat %s | ssh %s sudo devicemapper/exp/update-blocks.sh %s'%(self.blocknr_file, source_ip, self.src_file), shell=True)

class BlockMd5SyncPre(BlockMd5SyncExperiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        BlockMd5SyncExperiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

    def update_blocks(self) :
        sp.check_call('cat %s | ssh %s sudo devicemapper/exp/update-blocks.sh %s'%(self.blocknr_file, source_ip, self.src_file), shell=True)

class ZfsPre(ZfsExperiment) :
    def __init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag) :
        ZfsExperiment.__init__(self, size_gb, run, disk, update_percentage, transport, blocknr_file, tag)

    def update_blocks(self) :
        sp.check_call('cat %s | ssh %s sudo devicemapper/exp/update-blocks.sh %s'%(self.blocknr_file, source_ip, self.src_file), shell=True)

def perform_experiment(size_gb, update_percentage, runs, tag) :

    # Do CopyExperiment first to ensure source and destination are
    # identical before difference-based approaches are performed.
    methods = [CopyExperiment,
               BlockMd5SyncExperiment,
               RsyncExperiment,
               DsyncExperiment,
               ZfsExperiment
               ]

    fout = open('rsync-%s'%(tag), 'a')

    for m in methods :
        for disk in ['hdd','ssd'] :
            for transport in ['sshnone'] :
                for r in range(0, runs) :
                    exp = m(size_gb, r, disk, update_percentage, transport, "random", tag)
                    exp.setup()
                    begin_sec = time.time()
                    exp.sync()
                    end_sec = time.time()
                    print >> fout, 'method=%s transport=%s size_gb=%d update_percentage=%d disk=%s duration_sec=%.2f'%(exp, transport, size_gb, update_percentage, disk, end_sec-begin_sec)
                    exp.teardown()

def predetermined_blocks_experiment(size_gb, runs, tag) :

    fout = open('rsync-%s'%(tag), 'a')

    methods = [CopyPre, DsyncPre, RsyncPre, BlockMd5SyncPre, ZfsPre]
    disks = ['hdd', 'ssd']
    transports = ['sshnone']
    block_files = glob.glob('/mnt/myvg-data/home/thomas/tmp/prn_1_00.csv')
    update_percentage = None

    parameters = [methods,
                  disks,
                  transports,
                  block_files,
                  range(3)]

    for p in itertools.product(*parameters) :
        (m, disk, transport, block_file, run) = p
        exp = m(size_gb, run, disk, update_percentage, transport, block_file, tag)
        # exp.setup()
        begin_sec = time.time()
        # exp.sync()
        time.sleep(3)
        end_sec = time.time()
        modified_blocks=sp.check_output("wc -l %s | cut -f1 -d' '"%(block_file), shell=True)
        modified_blocks = modified_blocks.strip()
        print >> fout, 'method=%s transport=%s size_gb=%d disk=%s modified_blocks=%s block_file=%s duration_sec=%.2f'%(exp, transport, size_gb, disk, modified_blocks, block_file, end_sec-begin_sec)
        # exp.teardown()

def global_setup() :
    for ip in [source_ip, target_ip] :
        cmd = 'rsync -a /mnt/myvg-data/home/thomas/phd/devicemapper/code %s:~/devicemapper'%(ip)
        sp.call(shlex.split(cmd))
        cmd = 'rsync -a /mnt/myvg-data/home/thomas/phd/devicemapper/exp %s:~/devicemapper'%(ip)
        sp.call(shlex.split(cmd))

def main() :
    # use Unix epoch time to tag experiment run
    tag = int(time.time())

    # global_setup()
    sizes_gb = [8]
    update_percentages = [10,50,90]

    # for (size_gb, update_percentage) in itertools.product(sizes_gb, update_percentages) :
    #     perform_experiment(size_gb, update_percentage, 1, tag)
    predetermined_blocks_experiment(32, 1, tag)
        
if __name__ == '__main__' :
    main()
