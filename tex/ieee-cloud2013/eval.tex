\section{Evaluation}
\label{sec:eval}

%% The evaluation focuses on the following aspects:
%% \begin{itemize}
%% \item What is the impact of synchronizing multiple gigabytes of state on workloads running in parallel on the source and destination server?
%% \item How much does synchronization time improve for a typical usage scenario?
%% \item How does {\sysname} decrease resource utilization (CPU, disk I/O bandwidth, network bandwidth) compared to a traditional tool like, e.g., rsync?
%% \end{itemize}

The evaluation concentrates on the question of how much the synchronization time decreases by knowing the modified blocks in advance.
We compare {\sysname} with two other synchronization methods:
(a)~copy, and (b)~\verb+rsync+.
We describe each tool briefly in the following sections.

\subsection{Synchronization tools}

\subsubsection{scp/nc}

\verb+scp+, short for secure copy, copies entire files or directories over the network.
The byte stream is typically encrypted, putting additional load on the endpoint CPUs of the transfer.
Compression is optional and disabled for our evaluation.
The maximum throughput over a single encrypted stream we achieved with our benchmark systems was 55~MB/s.
This is half of the maximum throughput of a 1 gigabit Ethernet adapter.
If encryption is unnecessary, e.g.,
in a LAN environment,
\verb+nc+, short for netcat,
can be used instead.
%% With netcat we were able to saturate the gigabit link.
%% Due to space limitation we do not report on measurements with unencrypted network traffic.
Instead of netcat,
we used a patched version of \verb+ssh+ (the basis for \verb+scp+)~\footnote{\url{http://www.psc.edu/index.php/hpn-ssh}} which allows us to transfer data unencrypted.
Encryption is dropped after the initial secure handshake,
giving us a clearer picture of the CPU requirements for each workload.

\subsubsection{rsync}

\verb+rsync+ is used to synchronize two directory trees.
The source and destination can be remote in which case data is transferred over the network.
Network transfers are encrypted by default because \verb+rsync+ utilizes secure shell (\verb+ssh+) access to establish a connection between the source and destination.
If encryption is not desired,
the secure shell can be replaced by its unencrypted sibling, \verb+rsh+.
Instead of \verb+rsh+, we configured \verb+rsync+ to use the drop-in ssh replacement which supports unencrypted transfers.
\verb+rsync+ is smarter than \verb+scp+ because it employs several heuristics to minimize the transferred data.
For example, files are only transferred if the file's time stamp at the source is more recent than at the destination.
\verb+rsync+ also computes block-wise checksums at the source and destination.
Only if the checksums for a block are different is the data for this block transmitted to the destination.
For an in-depth description of the algorithm please refer to the work of~\citet{tridgell1996rsync}.
While \verb+rsync+ minimizes the amount of data sent over the network,
computing checksums for large files poses a high CPU overhead.
Also note, that the checksum computation takes place at the source \emph{and} destination.

\subsubsection{dsync}

Our synchronization tool, {\sysname}, differs from \verb+rsync+ in two main aspects:
(a)~{\sysname} is file-system agnostic because it operates on the block-level.
(b)~Instead of computing block-level checksums at the time of synchronization,
{\sysname} tracks the per-block modification status at runtime.
This obviates the need for checksum calculation between two subsequent synchronizations.
In addition to the kernel extensions, we implemented two userspace programs:
One to extract modified blocks based on the tracked information, called \verb+dmextract+.
Extracting modified blocks is done at the synchronization source.
The equivalent tool, which is run at the synchronization target, is called \verb+dmmerge+.
\verb+dmmerge+ reads a stream consisting of block numbers interleaved with block data.
The stream is merged with the target block device.
The actual network transfer is handled either by \verb+ssh+, if encryption is required,
or \verb+nc+, if encryption is unnecessary.

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{l|l|l}
%%                    & unencrypted & encrypted \\
%%       \hline
%%        copy        & netcat & scp \\
%%        rsync       & rsh    & ssh \\
%%        {\sysname}  & netcat & ssh \\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Linux tool used for unencrypted and encrypted network transfer.}
%%   \label{tab:transfer-matrix}
%% \end{table}

%% \begin{figure}
%%   \includegraphics{figs/synctime-vs-size}
%%   \caption{Synchronization time for three different synchronization techniques.
%% Lower is better. {\sysname} is the best in all cases.
%% The gap widens with increasing data set sizes.}
%%   \label{fig:rsync-time}
%% \end{figure}

\subsection{Setup}

Our benchmark setup consists of two machines with a 6-core AMD Phenom II processor,
a 2~TB spinning disk (Samsung HD204UI) as well as a 128~GB SSD (Intel SSDSC2CT12).
We limit the RAM available to the OS to 2~GB via kernel boot command-line parameters.
More memory would primarily be used for larger page and buffer caches.
This would hide the performance differences between HDDs and SSDs.
The machines are connected via Gigabit Ethernet and run a recent version of Ubuntu (12.04) with a 3.2 kernel.

\subsection{Benchmarks}

We used two types of benchmarks,
synthetic and realistic,
to evaluate {\sysname}'s performance and compare it with its competitors.
In our synthetic benchmark, we modified data blocks at random.
Each block had the same probability to be modified.
We always modified 10\% of the blocks.
Random modification is a worst case scenario because real world applications usually exhibit spacial locality with respect to the data they read and write.
Random block accesses lead to an increase in the number of I/O operations.
Conventional spinning hard disks have a tight limit on the number of input/output operations they can perform per second~(IOPS).
This IOPS limit is visible in the results.
We thus repeated the benchmarks with solid state drives and report numbers for both.

A second set of benchmarks involves the synchronization of virtual machine disks.
The virtual machines run the RUBiS server components~\cite{rubis}.
RUBiS is a an auction site prototype modeled after \url{eBay.com}.
The web application, written in PHP, consists of a web server,
and a data base backend.
A RUBiS client issues requests in 15 minute intervals.
Between each interval, the virtual machine is suspended,
and its state synchronized to a remote location.
We measure the synchronization time for each synchronization method described earlier.

\subsection{Synthetic benchmark results}

\begin{figure*}
  \begin{tabular}{cc}
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/1361221595-synctime-vs-size-hdd}
      \caption{Synchronization time for three different synchronization techniques.
Lower is better. Data on the source and target was stored on HDD.}
      \label{fig:synctime-vs-size-hdd}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/1361221595-synctime-vs-size-ssd}
      \caption{Synchronization time for three different synchronization techniques.
Lower is better. Data on the source and target was stored on SSD.}
      \label{fig:synctime-vs-size-ssd}
    \end{minipage}
  \end{tabular}
\end{figure*}


\begin{figure*}
  \begin{tabular}{cc}
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/1361221595-cpu-utilization}
      \caption{CPU utilization for a sample run of each synchronization tool.
100\% means all 6 cores are busy. 16\% means one core is busy.
Includes time spent in user and kernel mode as well as waiting for I/O.}
      \label{fig:cpu-utilization}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/1361221595-net-tx}
      \caption{Network transmit traffic on the sender side.
rsync and dsync transmit about the same amount of data in total, although the effective throughput of rsync is much lower.
copy transmits much more data than dsync and achieves throughput rates of 80~MB/s.}
      \label{fig:net-tx}
    \end{minipage}
  \end{tabular}
\end{figure*}

The synthetic benchmark measured synchronization times for variable sized data sets.
For each run we randomly modified 10\% of the total number of blocks.
The first set of results is shown in Figure~\ref{fig:synctime-vs-size-hdd}.
First of all,
we observe that the synchronization time increases linearly with the data set size;
irrespective of the synchronization method.
Second, \verb+rsync+ takes longest to synchronize the data, followed by {\sysname}, and copy.
The margins between the methods, however, vary substantially.
For example,
at 32~GB copy and {\sysname} perform almost identically,
while \verb+rsync+ takes more than five times as long (460 vs. 2450 seconds).
To our surprise, copying the entire state is slightly faster than extracting and merging the differences, 408 vs. 460 seconds for 32~GB.

We concluded that the random I/O operations were inhibiting {\sysname} to really shine.
Hence, we performed a second set of benchmarks where we used SSDs instead of HDDs.
The results are shown in Figure~\ref{fig:synctime-vs-size-ssd}.
While the increased random I/O performance of SSDs does not matter for \verb+rsync+,
its synchronization time is identical to the HDD benchmark,
SSDs enable {\sysname} to finish faster than copy.
At 32~GB {\sysname} takes 460~s on HDD and only 295~s on SSD.

%% Given the synchronization times and knowing the input data size,
%% we can calculate the throughput.
%% Note, however, that throughput is defined in terms of how long it takes to processes a certain amount of input data.
%% In the case of \verb+rsync+ and copy the input data is read completely exactly once.
%% So the definition of throughput is intuitively valid.
%% For {\sysname} though, only the modified parts are read and transmitted.
%% That is, the size of processed data is smaller than the total size.
%% In our synthetic benchmark,
%% we modified 10\% of the blocks,
%% hence the processed data size is $1/10$th of the input data.
%% In this light,
%% the throughput based on the processed data is $1/10$th of the throughput shown for {\sysname} in Figure~\ref{fig:tput-vs-size-nocache-nossd}.
%% For example, at 32~GB the throughput for {\sysname} based on the input size is ca. 40~MB/s.
%% Based on the data actually transmitted over the network,
%% the throughput is only 4~MB/s, however.

%% A plain copy achieves around 35~MB/s.
%% This is 1/3 of the theoretical bandwidth of gigabit Ethernet.
%% The limiting factor is CPU speed.
%% Our CPU cannot encrypt more than 35~MB of data per second.
%% Switching to a cheaper encryption, e.g., arcfour,
%% or abandoning encryption altogether would increase the throughput.
%% \verb+rsync+'s throughput is 20~MB/s for data sizes up to 4~GB,
%% but drops to 10~MB/s and less for larger data sets.
%% \verb+rsync+ is also CPU-bound due to its checksum computation.
%% Throughput for \verb+dsync+ as defined by the input data size is equivalent to copy's throughput.
%% If throughput is calculated based on the data's transfer size,
%% {\sysname}'s throughput with ca. 4~MB/s,
%% is worse than for \verb+rsync+.

The limiting factor for {\sysname} is random disk I/O.
Because our synthetic benchmark modifies blocks at random,
there is little spacial locality.
However, spacial locality is important to get good I/O performance from spinning media because only sequential access is fast.
Additional measurements show that the sender reads the non-contiguous data blocks from disk at 12~MB/s.
The receiver writes the same non-contiguous data blocks at 4-5~MB/s.
This explains why, if HDDs are involved,
copy finishes faster than {\sysname} although copy's transfer volume is 9x that of {\sysname}.

%% After identifying the bottleneck,
%% we performed a second benchmark run,
%% where all data was stored on a solid state disk.

To better highlight the differences between the three methods,
we also present CPU and network traffic traces in Figure~\ref{fig:cpu-utilization} and Figure~\ref{fig:net-tx}, respectively.
The trace was collected at the sender while synchronizing 32~GB from/to HDD.
The CPU utilization includes the time spent in kernel and user space, as well as waiting for I/O.
We observe that \verb+rsync+ is CPU-bound by its single-threaded checksum computation.
Up to $t=500$ the \verb+rsync+ sender process is idle,
while one core on the receiver-side computes checksums (not visible in the graph).
During \verb+rsync+'s second phase, one core, on our 6-core benchmark machine,
is busy computing and comparing checksums for the remaining 2000 seconds.
The network traffic during that time is minimal at less than 10~MB/s.
Copy's execution profile taxes the CPU much less:
utilization oscillates around 13\%.
On the other hand, it can be visually determined that copy's network traffic is much higher than for both \verb+rsync+ and {\sysname}.
Copy's network throughput is 80~MB/s on average.
{\sysname}'s execution profile uses double the CPU power of copy,
but only incurs a fraction of the network traffic.
The network throughput is limited by the write-rate at the receiver side.
The SSD cannot handle more than 15~MB/s of random writes.

%% The CPU utilization of copy is more erratic than both \verb+rsync+'s and dsync's.
%% The performance of copy is bound by the sequential disk read speed.
%% Looking at the network traffic, Figure~\ref{fig:net-tx},
%% it is evident that copy uses a lot more network bandwidth than either {\sysname} or rsync.
%% At ca. 10~MB/s {\sysname}'s network throughput is only 1/10th of the theoretical maximum.
%% The random disk I/O is the limiting factor for {\sysname}.
%% For workloads with higher spacial locality the effective throughput of dsync is higher.

\subsection{Realistic benchmark results}

While the worst case benchmark results are informative,
are also interested in the ``average'' case.
To do so, we performed the experiment in two stages:
first, we recorded block-level modifications to a virtual machine disk while running the RUBiS workload generator.
In a second step,
using the modified block numbers,
we performed timing measurements of how long each synchronization method takes.
Splitting the experiment into two phases allows us to repeat them either in combination or separately.

We argued earlier,
that a purely synthetic workload of random block modifications artificially constraints the performance of {\sysname}.
Although we already observed a 5x improvement in total synchronization time over \verb+rsync+,
the gain over copy was less impressive.
To highlight the difference between our synthetic and realistic benchmarks,
we plotted the number of consecutive modified blocks for each benchmark type.
Figure~\ref{fig:consecutive-blocks-cdf} prominently illustrates the difference in consecutive blocks.


\begin{figure}
  \includegraphics{figs/consecutive-blocks-frequency-vs-totalblocks}
  \caption{Difference in spacial locality between a synthetic and realistic benchmark run.
  In both cases 45k blocks are modified.
  For the synthetic benchmark 80\% are isolated individual blocks (36k at x=1).
  The realistic benchmark shows a higher degree of spacial locality, as observed, for example,
  by the jump from 2.5k (x=3) to 15k (x=4) blocks.}
  \label{fig:consecutive-blocks-cdf}
\end{figure}

We observe that 80\% of the modification involve only a single block (36k blocks at $x=1$ in Figure~\ref{fig:consecutive-blocks-cdf}).
In comparison, there are no single blocks for the RUBiS benchmark.
Every modification involves at least two consecutive blocks (1k blocks at $x=2$).
At the other end of the spectrum,
the longest run of consecutively modified blocks is 639 for the RUBiS benchmarks.
Randomly updated blocks rarely yield more than 5 consecutively modified blocks.
For the RUBiS benchmark, updates of 5 consecutive blocks happen most often:
the total number of modified blocks jumps from 2k to 15k moving from 4 to 5 consecutively modified blocks.

%% The synthetic benchmark has at most five consecutive modified blocks,
%% compared to 639 modified blocks in a row for the RUBiS benchmark.
%% Modifications of 4 consecutive blocks were most common for the realistic benchmark:
%% about 60\% of the modifications involved four consecutive blocks.
%% 16 kilobyte happens to be the size of the XXX data structure,
%% which is likely responsible for the high percentage of 4 consecutive blocks.

After motivating the insufficiency of a purely synthetic benchmark,
Figure~\ref{fig:synctime-vs-blocks-rubis-hdd} illustrates the results of our realistic workload.
We present numbers for the HDD case only because this workload is less constrained by the number of I/O operations per second.
%% This time, we present numbers only for the HDD case.
%% The numbers for SSDs are almost identical and show the same trends.
We performed the following steps 20 times:
(1)~boot virtual machine with RUBiS server, (2)~run RUBiS client emulator, and (3)~shut down virtual machine.
We traced all block modifications to the virtual machine disk between boot and shutdown.
The number of modified blocks was never the same between those 20 runs.
Instead, the number varies between 659 and 3813 blocks.
This can be explained by the randomness inherent in each RUBiS benchmark invocation.
The type and frequency of different actions,
e.g., buying an item or browsing the catalog,
is determined by chance.
Actions that modify the data base increase the modified block count.

%% Looking at each synchronization tool in isolation reveals little difference in synchronization time based on the number of modified blocks.
The synchronization time shows little variation between runs of the same method.
Copy transfers the entire 11~GB of data irrespective of actual modifications.
There should, in fact, be no difference based on the number of modifications.
\verb+rsync+'s execution time is dominated by checksum calculations.
%% Calculating checksums is also independent of the modified block count.
\verb+dsync+, however, transfers only modified blocks and should show variations.
The relationship between modified block count and synchronization time is just not discernible in Figure~\ref{fig:synctime-vs-blocks-rubis-hdd}.
Alternatively, we calculated the correlation coefficient for {\sysname} which is 0.54.
This suggests a positive correlation between the number of modified blocks and synchronization time.
The correlation is not perfect because factors other than the raw modified block count affect the synchronization time,
e.g., the spacial locality of updates.
%% Spacial locality of the modified blocks differs between the individual RUBiS runs.
%% Hence, the correlation between synchronization duration and modified block count for {\sysname} is not perfect.

The performance in absolute numbers is as follows:
\verb+rsync+, which is slowest, takes around 320 seconds to synchronize the virtual machine disk.
The runner up, copy, takes 200 seconds.
The clear winner,
with an average synchronization time of about 3 seconds,
is {\sysname}.
That is a factor 66x improvement over copy and more than 100x faster than \verb+rsync+.
{\sysname} reduces the network traffic to a minimum, like \verb+rsync+,
while being 100x faster.
Table~\ref{tab:performance-summary} summarizes the results.

\begin{figure}
  \includegraphics{figs/synctime-vs-blocks-rubis-hdd}
  \caption{Synchronization time for (a) copy, (b) rsync, and (c) dsync.
Block modifications according to the RUBiS workload.}
  \label{fig:synctime-vs-blocks-rubis-hdd}
\end{figure}

%% \begin{figure*}
%%   \begin{tabular}{cc}
%%     \begin{minipage}[t]{\columnwidth}
%%       \includegraphics{figs/tput-vs-size-hdd}
%%       \caption{Synchronization time for (a) copy, (b) rsync, and (c) dsync.
%% Block modifications according to the RUBiS workload.
%% Backing storage is a spinning disk (HDD).}
%%       \label{fig:synctime-vs-size-rubis-hdd}
%%     \end{minipage}
%%     &
%%     \begin{minipage}[t]{\columnwidth}
%%       \includegraphics{figs/tput-vs-size-ssd}
%%       \caption{Synchronization time for (a) copy, (b) rsync, and (c) dsync.
%% Block modifications according to the RUBiS workload.
%% Backing storage is a solid-state disk (SDD).}
%%       \label{fig:synctime-vs-size-rubis-ssd}
%%     \end{minipage}
%%   \end{tabular}
%% \end{figure*}

%% \subsection{Impact on workload}

%% \begin{figure}
%%   \includegraphics{figs/lorem}
%%   \caption{Impact of (1)~rsync and (2)~{\sysname} on request throughput running on the synchronization source. Throughput drops by xx \% for rsync, but only yy \% for {\sysname}.}
%%   \label{fig:rsync-progress}
%% \end{figure}

%% Show impact of rsync and {\sysname} on workload running in parallel on the synchronization source/target.
