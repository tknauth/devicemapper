\section{Implementation}
\label{sec:implementation}

%% \note{This can definitely be shortened if need be.}
Block devices are a well known abstraction in the Linux kernel.
A block device is a random-access data abstraction.
The fundamental addressable unit is a block.
A block usually has the same size as a page, 4 kilobyte ($2^{12}$ bytes).
The actual media underlying the block device may consist of even smaller units called sectors.
However, sectors are not addressable by themselves.
Sectors are usually 512 byte in size.
Hence, 8 sectors make up a block.
%% Block devices are in contrast to character devices.
%% Instead of random access,
%% character devices allow data access in a streaming fashion:
%% bytes can only be read or written sequentially -- one at a time.
%% One example of a character device is the keyboard.
%% Keystrokes can be read one after the other in the order the user types them.
%% Allowing access to a random keystroke in the user’s input is not sensible.
%% A prime example of a block device is, of course, the hard disk.
Usually, block devices are partitioned and formatted with a file system.
For more elaborate uses cases,
the Linux device mapper offers more flexibility to set up block devices.

%% Created a new Linux device mapper module.
%% The new device mapper module is based on the linear device mapper.
%% All I/O operations at the block level are interposed.
%% The write location is recorded in a bitset.
%% A procfs interface allows to access the bitset and read out the modified block numbers.
%% Two accompanying userspace tools have been implemented as well.
%% One extracts the modified blocks on the source, while the second tool merges the modified blocks at the receiver side.
%% Make use of fadvise system call to protect page cache on source/destination.

\subsection{Device mapper}
\label{sec:device-mapper}

The Linux device mapper is a powerful tool.
The device mapper, for example, allows to aggregate multiple individual block devices into single logical device.
In device mapper terminology, this is called a linear mapping.
In fact, logical devices can be constructed from arbitrary contiguous regions of existing block devices.
Besides simple aggregation, the device mapper also supports RAID configurations 0 (striping, no parity), 1 (mirroring), 5 (striping with parity), and 10 (mirroring and striping).
Another feature, which superficially looks like it solves our problem at hand, is snapshots.
Block devices can be “frozen” in time.
All modifications to the original device are re-directed to a special copy-on-write (COW) device.
Snapshots leave the original data untouched.
This allows, for example, to create consistent backups of a block device while still being able to service write requests for the same device.
If desired, the “external” modifications can later be merged into the original block device.
%% Figure xx illustrates the concept.
By applying the “external” modifications to a second (potentially remote) copy of the original device, this would solve our problem with zero implementation effort.
However, the solution lacks in two aspects.
First, additional storage is required to temporarily buffer all modifications.
The additional storage grows linearly with the number of modified blocks.
If, because of bad planning, the copy-on-write device is too small to hold all modifications,
the writes will be lost.
This is unnecessary to achieve what we are aiming for.
Second, because modifications are stored out-of-place, they must also be merged into the original data at the source of the actual copy; in addition to the destination.
Because of these limitations, we do not consider snapshots as a solution to our problem.
However, our solution utilizes the framework provided by the Linux device mapper and extends it.
Also note, that the related logical volume manager (LVM) is insufficient as well to solve our problem.
LVM offers snapshot functionality identical to that of the device mapper.
In fact, LVM is a device mapper wrapper with additional features.

%% http://smorgasbord.gavagai.nl/2010/03/online-merging-of-cow-volumes-with-dm-snapshot/
%% http://linuxgazette.net/114/kapil.html
%% http://www.kernel.org/doc/Documentation/device-mapper/snapshot.txt

\subsection{A Device Mapper Target}

The device mapper's functionality is split into separate targets.
Each target implements a predefined interface laid out in the \verb+target_type+~\footnote{\url{http://lxr.linux.no/linux+v3.6.2/include/linux/device-mapper.h\#L130}} structure.
The \verb+target_type+ structure is simply a collection of function pointers.
The target-independent part of the device mapper calls the target-dependant code through one of the pointers.
The most important functions are the constructor (\verb+ctr+), destructor (\verb+dtr+), and mapping (\verb+map+) function.
The constructor is called whenever a device of a particular target type is created.
Conversely, the destructor cleans up when a device is dismantled.
The userspace program to perform device mapper actions is called \verb+dmsetup+.
Through a series of ioctl calls,
information relevant to setup, tear down, and manage devices is exchanged between user and kernel space.
For example, 

\begin{verbatim}
# echo 0 1048576 linear /dev/original 0 | \
  dmsetup create mydev
\end{verbatim}

creates a new device called mydev.
Access to the sectors 0 through 1048576 of the mydev device are mapped to the same sectors of the underlying device \verb+/dev/original+.
The previously mentioned function, \verb+map+,
is invoked for every access to the linearly mapped device.
It applies the offset specified in the mapping.
The offset in our example is 0,
effectively turning the mapping into an identity function.

The device mapper has convenient access to all the information we need to track block modifications.
Every access to a mapped device passes through the \verb+map+ function.
We adapt the \verb+map+ function of the linear mapping mode for our purposes.

%% Linear mappings can involve more than one source device and the offsets for each source device can be different.
%% Translating between the constructed device and the underlying devices then becomes a two step process.
%% First, based on the sector of the constructed device, the underlying device is identified.

\subsection{Architecture}

Figure~\ref{fig:arch} shows a conceptional view of the layered architecture.
In this example we assume that the tracked block device forms the backing store of a virtual machine.
The lowest layer is the physical block device, for example, a hard disk.
The device mapper can be used to create a tracked device directly on top of the physical block device (Figure~\ref{fig:arch}, left).
The tracked block device is used instead of the physical device as a backing store for the VM.

Often, the backing store of a virtual machine is a file in the host's file system.
In these cases, a loopback device is used to convert the file into a block device on which the device mapper can in turn install the tracked device (Figure~\ref{fig:arch}, right).
The tracked device again functions as the VM's backing store.
The tracking functionality is entirely implemented in the host system kernel, i.e.,
the guests are oblivious of the tracking functionality.
The guest OS must not be modified and the tracking works with all guest operating systems.

\tikzstyle{mybox} = [draw=black!30,fill=gray!10, rectangle, rounded corners, inner sep=5pt, minimum width = 2.8cm, %% minimum height=1.5cm
]

\begin{figure}
  \begin{center}
    \begin{tikzpicture}[
        node distance = 0cm and 0cm 
      ]
      % \path[use as bounding box]
      % 	(-1.4,-2) rectangle (2.1,0.4);
      
      %% \path[draw=black!30,fill = gray!10, rounded corners] (0,0) rectangle (3,0.75);
      \node [mybox] (m1_1) at (0, 0) {virtual machine};
      \node [mybox,below=of m1_1] (m1_2) {device mapper};
      \node [mybox,below=of m1_2] (m1_3) {block device};

      \node [mybox,right=2cm of m1_3] (m2_5) {block device};
      \node [mybox,above=of m2_5] (m2_4) {file system};
      \node [mybox,above=of m2_4] (m2_3) {loopback device};
      \node [mybox,above=of m2_3] (m2_2) {device mapper};
      \node [mybox,above=of m2_2] (m2_1) {virtual machine};

      %% \path[draw=black!30,fill = gray!10, rounded corners] (1.2,-1) rectangle  (2.8,2.5);
      %% \path[draw=black!30,fill = gray!10, rounded corners] (3.2,-1) rectangle  (4.8,2.5);

    \end{tikzpicture}
    \caption{Architecture. Two configurations where the tracked block device is used by a virtual machine. If the VM used a file of the host system as its backing store, the loopback device turns this file into a block device (right).}
    \label{fig:arch}
  \end{center}
  
\end{figure}

\subsection{Data structure}

\begin{table*}
  \begin{center}
    \begin{tabular}{r@{ }|l||l|l|l|r@{ }}
      disk size & disk size  & bit vector size & bit vector size & bit vector size & bit vector size \\
                & (in bytes) & (in bit)        & (in bytes)      & (in pages)      & \\
      \hline
  4 KB & $2^{12}$ & $2^{0}$ & $2^0$ & $2^{0}$        &   1 bit\\
128 MB & $2^{27}$ & $2^{15}$ & $2^{12}$ & $2^{0}$    &   4 KB\\
  1 GB   & $2^{30}$ & $2^{18}$ & $2^{15}$ & $2^{3}$  &  64 KB\\
 16 GB & $2^{34}$ & $2^{22}$ & $2^{19}$ & $2^{7}$    & 512 KB\\
128 GB & $2^{37}$ & $2^{25}$ & $2^{22}$ & $2^{10}$   &   4 MB\\
256 GB & $2^{38}$ & $2^{26}$ & $2^{23}$ & $2^{11}$   &   8 MB\\
512 GB & $2^{39}$ & $2^{27}$ & $2^{24}$ & $2^{12}$   &  16 MB\\
  1 TB & $2^{40}$ & $2^{28}$ & $2^{25}$ & $2^{13}$   &  32 MB\\
    \end{tabular}
  \end{center}
  \caption{Relationship between data size and bit vector size. The accounting granularity is 4 KB, i.e., a single block or page.}
  \label{tab:bit-vector-size}
\end{table*}

Storing the modification status for a block requires exactly one bit:
if the bit is set, the block is modified. Otherwise, not.
The status bits of all blocks form a straightforward bit vector.
The bit vector is index by the block number.
However, given the size of today's hard disks and the option to attach multiple disks to a single machine,
the bit vector may occupy multiple megabytes of memory.
With 4~KB blocks, for example,
a bit vector of 64~MB is required to track the per-block modifications of a 2~TB disk.
An overview of the relationship between disk and bit vector size is provided in Table~\ref{tab:bit-vector-size}.

The total size of the data structure is not the only concern.
When allocating memory inside the kernel the absolute size of a single allocation is also constrained.
The kernel offers three different mechanisms to allocate memory:
(1)~\verb+kmalloc()+, (2)~\verb+__get_free_pages()+, and (3)~\verb+vmalloc()+.
However, only \verb+vmalloc()+ allows us to reliably allocate multiple megabytes of memory with a single invocation.
The various ways of allocating kernel memory are, for example, explained in the book ``Linux Device Drivers''~\cite{ldd3rd}.

%% The methods differ in three important properties:
%% \begin{itemize}
%% \item Is memory physically contiguous?
%% \item What is the maximum possible size of a single allocation?
%% \item What is the allocation cost? How is the allocation implemented internally?
%% \end{itemize}

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{l|*{5}{l}}
%%       pages & $2^{0}$ & $2^{1}$ & $2^{2}$ & \ldots & $2^{10}$ \\
%%       \hline
%%             & 379     & 0 & 0 & \ldots & 0\\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Availability of different allocation sizes.}
%%   \label{tab:buddyinfo}
%% \end{table}

%% \verb+kmalloc()+ returns physically contiguous memory but typically only supports allocations of up to 128~KB.
%% Larger allocations may be obtained using \verb+__get_free_pages()+.
%% However, availability of allocations larger than 128~KB is not guaranteed.
%% On a running Linux system, the availability of differently sized allocation units can be inspected by reading from \verb+/proc/buddyinfo+.
%% In theory, \verb+__get_free_pages()+ can allocate up to $2^{11}$ pages with a single invocation.
%% $2^{11}$ pages with 4~KB per page equates to 8~MB ($2^{12} * 2^{11} = 2^{23}$).
%% This would be sufficient to track a single 256~GB disk.
%% However, on systems running a production workload the limits will be much lower.
%% For example, the availability of different allocation sizes on the author's desktop is summarized in Table~\ref{tab:buddyinfo}.
%% The maximum size of a single allocation is, in fact, at the exact opposite of the spectrum:
%% \emph{a single page}.
%% The last candidate is \verb+vmalloc()+.
%% Although \verb+vmalloc()+ is more expensive in terms of work necessary for an allocation,
%% the main benefit is its ability to work around the lack of contiguous physical memory.
%% \verb+vmalloc()+ internally uses \verb+kmalloc()+ in combination with page table manipulations to construct a mapping that is \emph{contiguous in virtual memory}.
%% Allocations of multiple megabytes are thus possible with \verb+vmalloc()+.
%% The book Linux Device Drivers~\cite{ldd3rd} provides further details about the Linux kernel memory management.

Total memory consumption of the tracking data structures may still be a concern:
even commodity machines commonly provide up to 5 SATA ports.
Hard disk sizes of 2~TB are standard these days too.
The block-wise dirty status for a 10~TB setup like this requires 320~MB of memory.
We see two immediate ways to reduce the memory overhead:
(1)~increase the minimum unit size from a single block to 2, 4, or even more blocks.
(2)~replace the bit vector by a different data structure, e.g., a bloom filter.
A bloom filter could be configured to work with a fraction of the bit vector's size.
The trade-off is potential false positives and a higher (though constant) computational overhead when querying/changing the dirty status.

Our prototype currently does not persist the modified block status across reboots.
The state is held in memory and lost if the server looses power suddenly.
As part of the shutdown routine, however, a script can read the modification status from the user-level interface and persist it to a convenient location.

%% If total emory usage is of concern, a different technique for tracking the status of each block can be used.
%% The bit vector offers constant time set and query operations, while the bit vector's size grows linearly with the size of the disk.
%% Other data structures, e.g., linked lists or b-trees, offer different trade-offs.

\subsection{User-space interface}

The kernel extensions include support to export the relevant information to user space.
For each device registered with our customized device mapper,
there is a corresponding file in \verb+/proc+, e.g.,
\verb+/proc/mydev+.
Reading the file gives a human-readable list of block numbers which have been written.
Writing to the file resets the information, i.e.,
it clears the underlying bit vector.
The \verb+/proc+ file system integration uses the \verb+seq_file+ interface~\cite{salzman2001linux}.

Extracting the modified blocks from a block device is aided by a command line tool called \verb+dmextract+.
It takes as its only parameter the name of the device on which to operate, e.g.,
\verb+# dmextract mydevice+.
By convention, the block numbers for \verb+mydevice+ are read from \verb+/proc/mydevice+ and the block device is found at \verb+/dev/mapper/mydevice+.
The tool outputs, via standard out,
a sequence of $(block number, data)$ pairs.
Output can be redirect to a file, for later access,
or directly streamed over the network to the backup location.
The complementing tool for block integration is \verb+dmmerge+.
It reads, from standard input,
a stream of information as produced by \verb+dmextract+.
A single parameter points to the block device into which the blocks shall be integrated.

Following the Unix philosophy of chaining together multiple programs which all serve a single purpose well,
a command line to perform a remote backup may look like the following:
\begin{verbatim}
# dmextract mydev | \
  ssh remotehost dmmerge /dev/mapper/mydev
\end{verbatim}

This extracts the modifications from \verb+mydev+ on the local host,
copies the information over a secure channel to a remote host,
and merges the information on the remote host into a device also called \verb+mydev+.

%% \subsection{Combining with dm-snapshot}

%% Extending the kernel was necessary because the existing device mapper functionality did not fit our requirements.
%% Although it is possible to direct all modifications to a block device into a separate location (another block device),
%% this approach has drawbacks (cf. Section~\ref{sec:device-mapper}).
%% To achieve consistency with {\sysname} the data must not be modified while the updated blocks are extracted from the underlying device.
%% For example, while a virtual machine disk is synchronized, the virtual machine must not make any modifications to the device.
%% Shutting the VM down while the synchronization progresses is one option to ensure consistency.

%% If shutting down is not possible,
%% the previously mentioned snapshot support (cf.~\ref{}) may enable ``live'' delta-synchronizations:
%% updates to the tracked device must only be suspended briefly (less than one second).
%% During this brief period two things must happen atomically.
%% First, the tracked device $D$ is replaced by a snapshot device $S$.
%% Second, the information about which blocks were modified since the last synchronization must be preserved as well.
%% Set up correctly, all modifications are made directly on the tracked device.
%% Blocks written since the snapshot are preserved by copying them to the snapshot device.
%% The tracked device's state is kept as it was when the snapshot command was issued.
%% Modified blocks can be extracted from the snapshot and merged with another copy as usual.
%% All this without suspending the tracked device for the entire backup duration.

%% http://insights.oetiker.ch/linux/fadvise/
