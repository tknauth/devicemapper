\section{Implementation}
\label{sec:implementation}

%% \note{This can definitely be shortened if need be.}
A block device is a well known abstraction in the Linux kernel.
It provides random-access semantics to a linear array of blocks.
A block typically has the same size as a page, e.g., 4~KiB ($2^{12}$ bytes).
The actual media underlying the block device may consist of even smaller units called sectors.
However, sectors are not addressable by themselves.
Sectors are typically 512 byte in size.
Hence, 8 sectors make up a block.
%% Block devices are in contrast to character devices.
%% Instead of random access,
%% character devices allow data access in a streaming fashion:
%% bytes can only be read or written sequentially -- one at a time.
%% One example of a character device is the keyboard.
%% Keystrokes can be read one after the other in the order the user types them.
%% Allowing access to a random keystroke in the user’s input is not sensible.
%% A prime example of a block device is, of course, the hard disk.
Block devices are generally partitioned and formatted with a file system.
For more elaborate use cases,
the Linux device mapper offers more flexibility to set up block devices.

%% Created a new Linux device mapper module.
%% The new device mapper module is based on the linear device mapper.
%% All I/O operations at the block level are interposed.
%% The write location is recorded in a bitset.
%% A procfs interface allows to access the bitset and read out the modified block numbers.
%% Two accompanying userspace tools have been implemented as well.
%% One extracts the modified blocks on the source, while the second tool merges the modified blocks at the receiver side.
%% Make use of fadvise system call to protect page cache on source/destination.

\subsection{Device mapper}
\label{sec:device-mapper}

The Linux device mapper is a powerful tool.
The device mapper, for example, allows multiple individual block devices to be aggregated into a single logical device.
In device mapper terminology, this is called a linear mapping.
In fact, logical devices can be constructed from arbitrary contiguous regions of existing block devices.
Besides simple aggregation, the device mapper also supports RAID configurations 0 (striping, no parity), 1 (mirroring), 5 (striping with distributed parity), and 10 (mirroring and striping).
Another feature, which superficially looks like it solves our problem at hand, is snapshots.
Block devices can be “frozen” in time.
All modifications to the original device are re-directed to a special copy-on-write (COW) device.
Snapshots leave the original data untouched.
This allows, for example, to create consistent backups of a block device while still being able to service write requests for the same device.
If desired, the “external” modifications can later be merged into the original block device.
%% Figure xx illustrates the concept.
By applying the “external” modifications to a second (potentially remote) copy of the original device, this would solve our problem with zero implementation effort.

However, the solution lacks in two aspects.
First, additional storage is required to temporarily buffer all modifications.
The additional storage grows linearly with the number of modified blocks.
If, because of bad planning, the copy-on-write device is too small to hold all modifications,
the writes will be lost.
This is unnecessary to achieve what we are aiming for.
Second, because modifications are stored out-of-place, they must also be merged into the original data at the source of the actual copy; in addition to the destination.
Due to these limitations we consider device mapper snapshots as an inappropriate solution to our problem.

Because of the way the device mapper handles and interfaces with block devices,
our block-level tracking solution is built as an extension to it.
The next section describes how we integrated the tracking functionality into the device mapper,
%How we integrated the tracking functionality into the device mapper,
%is described in the next section.

% Also note, that the related logical volume manager (LVM) is insufficient as well to solve our problem.
% LVM offers snapshot functionality identical to that of the device mapper.
% In fact, LVM is a device mapper wrapper with additional features.

%% http://smorgasbord.gavagai.nl/2010/03/online-merging-of-cow-volumes-with-dm-snapshot/
%% http://linuxgazette.net/114/kapil.html
%% http://www.kernel.org/doc/Documentation/device-mapper/snapshot.txt

\subsection{A Device Mapper Target}

The device mapper's functionality is split into separate targets.
Various targets implementing, for example, RAID level 0, 1, and 5, already exist in the Linux kernel.
Each target implements a predefined interface laid out in the \verb+target_type+~\footnote{\url{http://lxr.linux.no/linux+v3.6.2/include/linux/device-mapper.h\#L130}} structure.
The \verb+target_type+ structure is simply a collection of function pointers.
The target-independent part of the device mapper calls the target-dependant code through one of the pointers.
The most important functions are the constructor (\verb+ctr+), destructor (\verb+dtr+), and mapping (\verb+map+) functions.
The constructor is called whenever a device of a particular target type is created.
Conversely, the destructor cleans up when a device is dismantled.
The userspace program to perform device mapper actions is called \verb+dmsetup+.
Through a series of \verb+ioctl()+ calls,
information relevant to setup, tear down, and device management is exchanged between user and kernel space.
For example, 

\begin{verbatim}
# echo 0 1048576 linear /dev/original 0 | \
  dmsetup create mydev
\end{verbatim}

\noindent%
creates a new device called mydev.
Access to the sectors 0 through 1048576 of the mydev device are mapped to the same sectors of the underlying device \verb+/dev/original+.
The previously mentioned function, \verb+map+,
is invoked for every access to the linearly mapped device.
It applies the offset specified in the mapping.
The offset in our example is 0,
effectively turning the mapping into an identity function.

The device mapper has convenient access to all the information we need to track block modifications.
Every access to a mapped device passes through the \verb+map+ function.
We adapt the \verb+map+ function of the linear mapping mode for our purposes.

%% Linear mappings can involve more than one source device and the offsets for each source device can be different.
%% Translating between the constructed device and the underlying devices then becomes a two step process.
%% First, based on the sector of the constructed device, the underlying device is identified.

\subsection{Architecture}

Figure~\ref{fig:arch} shows a conceptual view of the layered architecture.
In this example we assume that the tracked block device forms the backing store of a virtual machine (VM).
The lowest layer is the physical block device, for example, a hard disk.
The device mapper can be used to create a tracked device directly on top of the physical block device (Figure~\ref{fig:arch}, left).
The tracked block device replaces the physical device as the VM's backing store.

\tikzstyle{mybox} = [draw=black!30,fill=gray!10, rectangle, rounded corners, inner sep=5pt, minimum width = 2.8cm, %% minimum height=1.5cm
]

\begin{figure}[t]
  \begin{center}
    \begin{tikzpicture}[
        node distance = 0cm and 0cm 
      ]
      % \path[use as bounding box]
      % 	(-1.4,-2) rectangle (2.1,0.4);
      
      %% \path[draw=black!30,fill = gray!10, rounded corners] (0,0) rectangle (3,0.75);
      \node [mybox] (m1_1) at (0, 0) {virtual machine};
      \node [mybox,below=of m1_1] (m1_2) {device mapper};
      \node [mybox,below=of m1_2] (m1_3) {block device};

      \node [mybox,right=2cm of m1_3] (m2_5) {block device};
      \node [mybox,above=of m2_5] (m2_4) {file system};
      \node [mybox,above=of m2_4] (m2_3) {loopback device};
      \node [mybox,above=of m2_3] (m2_2) {device mapper};
      \node [mybox,above=of m2_2] (m2_1) {virtual machine};

      %% \path[draw=black!30,fill = gray!10, rounded corners] (1.2,-1) rectangle  (2.8,2.5);
      %% \path[draw=black!30,fill = gray!10, rounded corners] (3.2,-1) rectangle  (4.8,2.5);

    \end{tikzpicture}
    \caption{Two configurations where the tracked block device is used by a virtual machine (VM). If the VM used a file of the host system as its backing store, the loopback device turns this file into a block device (right).}
    \label{fig:arch}
  \end{center}
  
\end{figure}

Often, the backing store of a virtual machine is a file in the host's filesystem.
In these cases, a loopback device is used to convert the file into a block device.
Instead of tracking modifications to a physical device,
we track modifications to the loopback device (Figure~\ref{fig:arch}, right).
The tracked device again functions as the VM's backing store.
The tracking functionality is entirely implemented in the host system kernel, i.e.,
the guests are unaware of the tracking functionality.
The guest OS does not need to be modified, and the tracking works with all guest operating systems.

\subsection{Data structure}

%%%
%%%	Observation:  Sadly, the use of KiB vs KB, MiB vs MB etc.,  is now commonplace 
%%%	for units based on 2^n sizes.  Is this something you _want_ to change?  Yes,
%%%	it's _annoying_
%%%

\begin{table*}
  \begin{center}
    \begin{tabular}{r@{ }|l||l|l|l|r@{ }}
      Disk size & Disk size  & Bit vector size & Bit vector size & Bit vector size & Bit vector size \\
                & (bytes) & (bits)        & (bytes)      & (pages)      & \\
      \hline
  4 KiB & $2^{12}$ & $2^{0}$ & $2^0$ & $2^{0}$        &    1 bit\\
128 MiB & $2^{27}$ & $2^{15}$ & $2^{12}$ & $2^{0}$    &    4 KiB\\
  1 GiB   & $2^{30}$ & $2^{18}$ & $2^{15}$ & $2^{3}$  &   64 KiB\\
%  16 GiB & $2^{34}$ & $2^{22}$ & $2^{19}$ & $2^{7}$    &  512 KiB\\
% 128 GiB & $2^{37}$ & $2^{25}$ & $2^{22}$ & $2^{10}$   &    4 MiB\\
% 256 GiB & $2^{38}$ & $2^{26}$ & $2^{23}$ & $2^{11}$   &    8 MiB\\
512 GiB & $2^{39}$ & $2^{27}$ & $2^{24}$ & $2^{12}$   &   16 MiB\\
  1 TiB & $2^{40}$ & $2^{28}$ & $2^{25}$ & $2^{13}$   &   32 MiB\\
  4 TiB & $2^{42}$ & $2^{30}$ & $2^{27}$ & $2^{15}$   &  128 MiB\\
    \end{tabular}
  \end{center}
  \caption{Relationship between data size and bit vector size. The accounting granularity is 4 KiB, i.e., a single block or page.}
  \label{tab:bit-vector-size}
\end{table*}

Storing the modification status for a block requires exactly one bit:
%if the bit is set, the block is modified. Otherwise, not.
%if the bit is set, the block has been modified, otherwise the bit remains. Otherwise, not.
a set bit denotes modified blocks, unmodified blocks are represented by an unset bit.
The status bits of all blocks form a straightforward bit vector.
The bit vector is indexed by the block number.
%However, 
Given the size of today's hard disks and the option to attach multiple disks to a single machine,
the bit vector may occupy multiple megabytes of memory.
With 4~KiB blocks, for example,
a bit vector of 128~MiB is required to track the per-block modifications of a 4~TiB disk.
An overview of the relationship between disk and bit vector size is provided in Table~\ref{tab:bit-vector-size}.

The total size of the data structure is not the only concern when
allocating memory inside the kernel;
the size of a single allocation is also constrained.
The kernel offers three different mechanisms to allocate memory:
(1)~\verb+kmalloc()+, (2)~\verb+__get_free_pages()+, and (3)~\verb+vmalloc()+.
However, only \verb+vmalloc()+ allows us to reliably allocate multiple megabytes of memory with a single invocation.
The various ways of allocating Linux kernel memory are detailed in ``Linux Device Drivers''~\cite{ldd3rd}.

%% The methods differ in three important properties:
%% \begin{itemize}
%% \item Is memory physically contiguous?
%% \item What is the maximum possible size of a single allocation?
%% \item What is the allocation cost? How is the allocation implemented internally?
%% \end{itemize}

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{l|*{5}{l}}
%%       pages & $2^{0}$ & $2^{1}$ & $2^{2}$ & \ldots & $2^{10}$ \\
%%       \hline
%%             & 379     & 0 & 0 & \ldots & 0\\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Availability of different allocation sizes.}
%%   \label{tab:buddyinfo}
%% \end{table}

%% \verb+kmalloc()+ returns physically contiguous memory but typically only supports allocations of up to 128~KB.
%% Larger allocations may be obtained using \verb+__get_free_pages()+.
%% However, availability of allocations larger than 128~KB is not guaranteed.
%% On a running Linux system, the availability of differently sized allocation units can be inspected by reading from \verb+/proc/buddyinfo+.
%% In theory, \verb+__get_free_pages()+ can allocate up to $2^{11}$ pages with a single invocation.
%% $2^{11}$ pages with 4~KB per page equates to 8~MB ($2^{12} * 2^{11} = 2^{23}$).
%% This would be sufficient to track a single 256~GB disk.
%% However, on systems running a production workload the limits will be much lower.
%% For example, the availability of different allocation sizes on the author's desktop is summarized in Table~\ref{tab:buddyinfo}.
%% The maximum size of a single allocation is, in fact, at the exact opposite of the spectrum:
%% \emph{a single page}.
%% The last candidate is \verb+vmalloc()+.
%% Although \verb+vmalloc()+ is more expensive in terms of work necessary for an allocation,
%% the main benefit is its ability to work around the lack of contiguous physical memory.
%% \verb+vmalloc()+ internally uses \verb+kmalloc()+ in combination with page table manipulations to construct a mapping that is \emph{contiguous in virtual memory}.
%% Allocations of multiple megabytes are thus possible with \verb+vmalloc()+.
%% The book Linux Device Drivers~\cite{ldd3rd} provides further details about the Linux kernel memory management.

Total memory consumption of the tracking data structures may still be a concern:
even commodity (consumer) machines commonly provide up to 5 SATA ports for attaching disks.
Hard disk sizes of 4~TB are standard these days too.
%The block-wise dirty status for a 10~TB setup like this requires 320~MB of memory.
To put this in context, 
the block-wise dirty status for a 10~TiB setup requires 320~MiB of memory.
We see two immediate ways to reduce the memory overhead:
\begin{enumerate}
\item Increase the minimum unit size from a single block to 2, 4, or even more blocks.
%% Do this dynamically?  Make it a tuneable?
\item Replace the bit vector by a different data structure, e.g., a bloom filter.
\end{enumerate}
\noindent A bloom filter could be configured to work with a fraction of the bit vector's size.
The trade-off is potential false positives and a higher (though constant) computational overhead when querying/changing the dirty status.
We leave the evaluation of tradeoffs introduced by bloom filters for future work.

Our prototype currently does not persist the modification status across reboots.
Also, the in-memory state is lost, if the server suddenly loses power.
One possible solution is to persist the state as part of the server's regular shutdown routine.
During startup, the system initializes the tracking bit vector with the state written at shutdown.
%% MIKEC:  This is somewhat similar to what happens with battery-backed write cache on hardware RAID controllers
If the initialization state is corrupt or not existing,
each block is marked ``dirty'' to force a full synchronization.
%% MIKEC:  Observation: In theory, at least, you could always layer a per-file dirty flag and fall back to that i.e. a hybrid system

%% If total emory usage is of concern, a different technique for tracking the status of each block can be used.
%% The bit vector offers constant time set and query operations, while the bit vector's size grows linearly with the size of the disk.
%% Other data structures, e.g., linked lists or b-trees, offer different trade-offs.

\subsection{User-space interface}

The kernel extensions export the relevant information to user space.
For each device registered with our customized device mapper,
there is a corresponding file in \verb+/proc+, e.g.,
\verb+/proc/mydev+.
Reading the file gives a human-readable list of block numbers which have been written.
Writing to the file resets the information, i.e.,
it clears the underlying bit vector.
%%% Ummm..... isn't this going to be dangerous?  i.e. testing use only for prototype :-)
The \verb+/proc+ file system integration uses the \verb+seq_file+ interface~\cite{salzman2001linux}.

Extracting the modified blocks from a block device is aided by a command line tool called \verb+dmextract+.
The \verb+dmextract+ tool takes as its only parameter the name of the device on which to operate, e.g.,
\verb+# dmextract mydevice+.
By convention, the block numbers for \verb+mydevice+ are read from \verb+/proc/mydevice+ and the block device is found at \verb+/dev/mapper/mydevice+.
The tool outputs, via standard out,
a sequence of $(block number, data)$ pairs.
Output can be redirected to a file, for later access,
or directly streamed over the network to the backup location.
The complementing tool for block integration, \verb+dmmerge+,
%It reads, from standard input,
reads 
a stream of information as produced by \verb+dmextract+
from standard input,
A single parameter points to the block device into which the changed blocks shall be integrated.

Following the Unix philosophy of chaining together multiple programs which each serve a single purpose well,
a command line to perform a remote backup may look like the following:
\begin{verbatim}
# dmextract mydev | \
  ssh remotehost dmmerge /dev/mapper/mydev
\end{verbatim}
%% If this is breaking over columns/pages, look at the fancyvrb package 
%% 	For preamble:
%%		\usepacakge{fancyvrb}
%% then
%% 	\begin{Verbatim}[samepage=true]
%% 	# dmextract mydev | \
%%	   ssh remotehost dmmerge /dev/mapper/mydev
%% 	\end{Verbatim}

This extracts the modifications from \verb+mydev+ on the local host,
copies the information over a secure channel to a remote host,
and merges the information on the remote host into an identically named device.

%% \subsection{Combining with dm-snapshot}

%% Extending the kernel was necessary because the existing device mapper functionality did not fit our requirements.
%% Although it is possible to direct all modifications to a block device into a separate location (another block device),
%% this approach has drawbacks (cf. Section~\ref{sec:device-mapper}).
%% To achieve consistency with {\sysname} the data must not be modified while the updated blocks are extracted from the underlying device.
%% For example, while a virtual machine disk is synchronized, the virtual machine must not make any modifications to the device.
%% Shutting the VM down while the synchronization progresses is one option to ensure consistency.

%% If shutting down is not possible,
%% the previously mentioned snapshot support (cf.~\ref{}) may enable ``live'' delta-synchronizations:
%% updates to the tracked device must only be suspended briefly (less than one second).
%% During this brief period two things must happen atomically.
%% First, the tracked device $D$ is replaced by a snapshot device $S$.
%% Second, the information about which blocks were modified since the last synchronization must be preserved as well.
%% Set up correctly, all modifications are made directly on the tracked device.
%% Blocks written since the snapshot are preserved by copying them to the snapshot device.
%% The tracked device's state is kept as it was when the snapshot command was issued.
%% Modified blocks can be extracted from the snapshot and merged with another copy as usual.
%% All this without suspending the tracked device for the entire backup duration.

%% http://insights.oetiker.ch/linux/fadvise/
