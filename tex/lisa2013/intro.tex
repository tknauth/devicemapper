\section{Introduction}
\label{sec:introduction}

``Everything fails all the time.'' is the modus operandi when it comes to provisioning critical IT infrastructure.
Although the definition of critical is up for debate, redundancy is key to achieving high availability.
Redundancy can be added at many levels.
For example, at the hardware level, deploying two network cards per server
can allow one network card to fail, yet the server will still be reachable.
Performance may be degraded due to the failure but the server is still available.

%% What is the problem?
Adding hardware redundancy is just one piece of the availability puzzle.
To ensure data availability in the presence of failures,
the data must be replicated.
However, synchronizing tens, hundreds, or even thousands of gigabyte of data across the network is expensive.
It is expensive in terms of network bandwidth, if a na\"{\i}ve  copy-everything approach is used.
It is also expensive in terms of CPU cycles, if a checksum-based delta-copy approach is used.
Although a delta-copy minimizes network traffic,
it relies on a mechanism to identify differences between two versions of the data in question.
Determining the differences after the fact is less efficient than recording modifications while they are happening.

%% Why is it interesting and important?
% Making synchronization of data sets resource-efficient and fast is important in a world where ``everything fails all the time'' according to Amazon's CTO Werner Vogels.
% One copy is simply not enough.
% To stay with Amazon: the lowest replication factor for data stored in S3,
% Amazon's Simple Storage Service,
% is three (3).
% Three is also the recommended number of copies for data stored at home:
% the original, one external copy, and another copy stored at a remote location.
% This is to protect against all kinds of incidents, including,
% theft, hardware failures, disaster recovery, and accidental data deletion.

%% Why is it hard? (E.g., why do naive approaches fail?)  naive
One problem with synchronizing large amounts of data, e.g., for backups,
is that the backup operation takes on the order of minutes to hours.
As data sets continue to grow,
consumer drives now hold up to 4~TB,
so does the time required to synchronize them.
% For example, a recent Western Digitial drive such as WD4000F9YZ
For example, just reading 4~TB stored on a single spinning disk takes more than 6 hours~\cite{rosenthal2010keeping}.
Copying hundreds of gigabytes over a typical wide area network for remote backups will proceed at a fraction of the previously assumed 170~MB/s.
Ideally, the time to synchronize should be independent of the data size;
with the size of updated/added data being the main factor influencing synchronization speed.

%% Why hasn't it been solved before? (Or, what's wrong with previous
%% proposed solutions? How does mine differ?)
The key insight is,
that between two synchronizations of a data set,
most of the data is unmodified.
Hence, it is wasteful to copy the entire data set.
Even if the data sets are almost identical,
the differences have to be determined.
This is done, for example, by computing block-wise checksums.
Only blocks with mismatching checksums are transferred.
Instead of detecting changes after the fact,
we propose to track and record them at run time.
Online tracking obviates checksum computations,
while still only transferring the changed parts of the data.
The benefits of online modification recording are plentiful:
(1)~minimizes network traffic,
(2)~no CPU cycles spent on checksum computation,
(3)~minimizes the data read from and written to disk, and
(4)~minimizes page cache pollution.

%% What are the key components of my approach and results? Also
%% include any specific limitations.
We implemented a prototype of our synchronization solution, named {\sysname}, on Linux.
It consists of a kernel modification and two complimentary userspace tools.
The kernel extension tracks modifications at the block device level.
The userspace tools,
\verb+dmextract+ and \verb+dmmerge+,
allow for easy extraction and merging of the modified block level state.

%% \subsection{Summary of Contributions}
To summarize, in this paper, we make the following contributions:

\begin{itemize}
\item Identify the need for better mechanisms to synchronize large, binary data blobs across the network.
\item Propose an extension to the Linux kernel to enable efficient synchronization of large, binary data blobs.
\item Extend the Linux kernel with block-level tracking code.
\item Provide empirical evidence to show the effectiveness of our improved synchronization method.
\item We share with the scientific community all code, measurements, and related artifacts.
  We encourage other researchers to replicate and improve our findings.
The results are available at \url{https://bitbucket.org/tknauth/devicemapper/}.
\end{itemize}
