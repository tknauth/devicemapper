\section{Problem}
\label{sec:problem}

The task at hand is to periodically synchronize two physically distant data sets
and to do so efficiently.
The qualifier ``periodically'' is important because there is little to optimize for a one-off synchronization.
With periodic synchronizations,
on the other hand,
we can potentially exploit the typical case where the majority of the data is unmodified between successive synchronizations.

There exists no domain-specific knowledge about the data being synchronized,
i.e., we have to treat it as a binary blob.
Using domain-specific knowledge,
such as file system meta-data,
alternative optimizations are possible.
Synchronization tools routinely use a file's last modified time to check whether to consider it for a possible transfer.

We are primarily interested in large data sizes of multiple giga- to terabytes.
The techniques we present are also applicable to smaller sizes,
but the problems we solve with our system are more pressing when data sets are large.
One example in the cloud computing environment are virtual machine disks.
Virtual machine disks change as a result of the customer starting a virtual machine,
performing computation,
storing the result,
and shutting the virtual machine down again.
As a result of the users' actions,
the disk's contents change over time.
However, only a fraction of the entire disk is actually modified.
It is the cloud provider's responsibility to store the virtual machine disk in multiple locations,
e.g., for fault tolerance.
If one data center becomes unavailable,
the customer can restart their virtual machine in a backup data center.
For example, a cloud provider may synchronize virtual machine disks once per day between two data centers A and B.
If data center A becomes unavailable,
data center B has a copy which is at most 24 hours out of date.
If customers need more stringent freshness guarantees,
the provider may offer alternative backup solutions to the ones considered in this paper.

%% \subsection{Use case}

%% \begin{figure}
%%   \includegraphics{figs/lorem}
%%   \caption{Request inter-arrival times for three web services. Idle times of multiple seconds are frequent and allow to save resources.}
%%   \label{fig:interarrival}
%% \end{figure}

Copying the entire data is a simple and effective way to achieve synchronization.
Yet, it generates a lot of gratuitous network traffic, which is unacceptable.
Assuming an unshared 10 Gigabit Ethernet connection, transferring 100 GB takes about 83 seconds (in theory anyway and assuming an ideal throughput of 1.2~GB/s).
However, 10 Gigabit Ethernet equipment is still much more expensive than commodity Gigabit Ethernet.
While 10 Gigabit may be deployed inside the data center,
wide-area networks with 10 Gigabit are even rarer.
Also, network links will be shared among multiple participants -- be they data streams of the same applications, different users, or even institutions.

The problem of transmitting large volumes of data over constrained long distance links,
is exacerbated by continuously growing data sets and disk sizes.
Offsite backups are important to provide disaster recovery and business continuity in case of site failures.

Instead of indiscriminatly copying everything, we need to identify the changed parts.
Only the changed parts must actually be transmitted over the network.
Tools, such as \verb+rsync+, follow this approach.
The idea is to compute one checksum for each block of data at the source and destination.
Only if there is a checksum mismatch for a block,
is the block transferred.
While this works well for small data sizes,
the checksum computation is expensive if data sizes reach into the gigabyte range.

As pointed out earlier,
reading multiple gigabytes from disks takes on the order of minutes.
Disk I/O operations and bandwidth are occupied by the synchronization process and unavailable to production workloads.
Second, checksum computation is CPU-intensive.
%% For example,
%% on our benchmark systems it takes XXX seconds to compute the MD5 hash of a 32~GB file.
%% The checksum computation progresses at a rate of XX~MB/s,
%% about $X/Y$th of the maximum available disk throughput.
For the duration of the synchronization,
one entire CPU is dedicated to computing checksums,
and unavailable to the production workload.
Third, reading all that data from disk interferes with the system's page cache.
The working set of running processes is evicted from memory,
only to make place for data which is used exactly \emph{once}.
Applications can give hints to the operating system to optimize the caching behavior~\cite{rsync-fadvise-patch}.
However, this is not a perfect solution either, as the OS is free to ignore the advice if it cannot adhere to it.
In addition, the application developer must be aware of the problem to incorporate hints into the program.

All this is necessary because there currently is no way of identifying changed blocks without comparing their checksums.
Our proposed solution,
which tracks block modifications as they happen,
extends the Linux kernel to do just that.
The implementation details and design considerations form the next section.

%% of the Synchronizing of huge (tens to hundreds of gigabytes) data sets is currently expensive in terms of mainly three factors: (1) CPU cycles, (2) I/O bandwidth, and (3) page cache pollution.
%% One simple solution is to copy the primary to the backup location in full every time.
%% With data sets in the three digit GB range the network quickly becomes a bottleneck.
%% to the problem.
%% In order to synchronize the primary with the backup copy, Synchronization of large (multiple gigabyte) virtual machine disks over the network (e.g., for disaster recovery).
%% Traditional delta-synchronization by block-wise checksum comparison is a resource intensive process.
%% Block-wise checksum, however, are necessary because the blocks modified since the last synchronization are not know.
%% Instead of calculating checksum, we propose to track block modifications at runtime.

%% Online recording of block modifications.
%% No need to compute block-wise checksums.
%% When synchronizing, only copy recorded/modified blocks.
%% Saves bandwidth, CPU, working set (page cache).
