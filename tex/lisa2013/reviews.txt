===========================================================================
LISA '13 Review #4A
Updated Monday 10 Jun 2013 1:59:40am PDT
---------------------------------------------------------------------------
Paper #4: dsync: Efficient Block-wise Synchronization of Multi-Gigabyte
Binary Data
---------------------------------------------------------------------------

Overall merit: 4. Accept
Reviewer expertise: 3. Knowledgeable

===== Paper summary =====

This paper describes a mechanism for synchronising large volumes of data by tracking write/update activity on storage devices at the block device level.  By tracking operations at the block device level, the mechanism is agnostic with respect to file-system design and semantics.  An implementation of the mechanism is described, and results demonstrating improvements in I/O volumes, CPU cost, and time-to-synchronise are presented.  The implementation is freely available for validation by other researches.

===== Comments for author =====

Thank you for your submission. 

* In s2, you write "Third, reading all that data from disk interferes with the system's page cache.  The working set of running processes is evicted from memory, only to make place for data which is used exactly once."  Given citation [1], I was expecting to see a note here (in addition to the Related Work section) that this particular cost is avoidable or at least able to be reduced to a smaller impact with specific support being requested by user-level programs.  If you wanted to take this further, noting the additional I/O cost of flagging a file in this way might help the reader understand that even posix_fadvise is not a perfect solution.

* In s4.1.1 where you review scp, you describe a maximum throughput of 55MB/s for scp over SSH encrypted connections, but do not detail the cryptographic configuration used (hardware is detailed in s4.2).  There also seems to be an implicit assumption that disk I/O isn't a predominant factor.  The throughput of SSH with encryption enabled varies with the choice of cipher; if you wish to establish a practical comparative baseline then a more detailed description is needed here.  We do note that your evaluation was effectively using a null operation and that this _is_ clearly explained, but for system administrators attempting to understand just how much benefit your system derives compared to their usual approach, the 55MB/s figure could be misconstrued.  Similarly, there is other detail that could be clarified here.  A network throughput of 55MB/s isn't going to be solely limited by CPU; a poor switching fabric could act to reduce throughput in a way which isn't immediately obvious at Layer 7, a poorly compiled version of SSH (or a version which makes use of hardware acceleration) would also give different results.  Approaches which might help to validate throughput are use of a back-to-back cable (i.e. no switch), use of virtualisation [has other consequences] and verifying error rates and window sizes for your baseline transfer. Again, without meaning to pre-empt your results, this section might benefit from a quick statement of the baseline throughput that _was_ achieved by your patched ssh.

* In s4.1.2 describing rsync you state that "... files are only transferred if the file's timestamp at the source is more recent than at the destination.".  That's not the case (as your subsequent statements seem to support).  The following script will demonstrate (tested against rsync 3.0.9, default options):

#!/bin/sh
# Verify file-modification behavior of rsync
mkdir rsync rsync/src rsync/dest
echo "first" > rsync/src/file.txt
sleep 1   # not strictly necessary
echo "second" > rsync/dest/file.txt
rsync -vv rsync/src/* rsync/dest/

Rsync can't rely on modification times alone; it has no visibility on time synchronisation between two hosts.  Implicitly, the command 'rsync src dest' is directing rsync to ensure that the contents of dest reflect src -- subject to tweaking semantics with command-line arguments.  Given your main "competition" as presented is rsync, you probably need to give a little more coverage of how rsync works, what it does to minimise overheads, and then directly address these in your related work/conclusion.  

* In s4.1.3 you claim difference between dsync and rsync in two respects.  I think there's another important difference.  dsync is "more" unidrectional than rsync.  Resolving tracked block-change information between two hosts is a harder problem than resolving differences between files on a filesystem.  [Yes, I agree this isn't the problem you are trying to solve :-)]  I'm noting this only because the performance gains which dsync presents come at a cost, and this is one such cost.  You also don't get the "luxury" of access to *any* time data when working at the block level.  You *must* assume the target is "older" than the source.

* Questions re: s4.3:
- In what cases does dsync become _less_ effective than rsync (e.g. in terms of #writes, volume, etc)
- What is the rationale behind modifying 10% of blocks?  Equivalent # of operations?  Volume?

* Questions re: s4.4:
- "The SSD cannot handle more than 15MB/s of random writes" doesn't match with Intel's specs which are 52k IOP writes of a 4k block (about 200MB/s, I think).  15MB => 15 * 256 blocks @ 4K = ~4000 IOPs.   Is something else happening here?  TRIM?

* In s5 your description of DRBD assumes operation in single-primary mode.  That compares well with dsync, but for completeness you might want to contrast with dual-primary mode (requires filesystem with concurrent access semantics, etc).  

****  Two general comments:
** 1)  is that your related work section feels rushed -- there's plenty of other work that might be useful to establish where dsync sits.  Measuring results against rsync is good (sysadmins know rsync, and rsync is initiated from the user-level) but I'd like to see more a more detailed comparison of dsync with DRBD as there's much in common with DRBD's motivation.  
** 2) is that your results don't push your system to its limits to see where it breaks.  rsync's performance limitations are generally well understood.  This might be an area for more research, but could also be an interesting area for a presentation [and if you've gone to that effort..... hint hint]


A few other (very minor) suggestions which you may wish to consider:

* In s3.5, final paragraph you write about "Following the Unix philosophy of chaining together multiple programs which all serve a single purpose well ...".  Suggest replacing ALL with EACH.

* Through your paper you use 'spacial locality'; I believe you mean 'spatial locality'

* Switching off encryption to get results which aren't obfusgated by cryptographic overhead makee sense for testing, but it would be worth noting that in practise, leaving encryption _on_ helps detect data corruption (malicious or otherwise) in transit.  Kind of important in this particular use-case...

===========================================================================
LISA '13 Review #4B
Updated Monday 10 Jun 2013 8:17:48pm PDT
---------------------------------------------------------------------------
Paper #4: dsync: Efficient Block-wise Synchronization of Multi-Gigabyte
Binary Data
---------------------------------------------------------------------------

Overall merit: 4. Accept
Reviewer expertise: 3. Knowledgeable

===== Paper summary =====

The authors describe a method for tracking block level changes to filesystems and then performing incremental replication operations.

===== Comments for author =====

I like this paper overall; it is a nice implementation of a simple idea. I'm not sure about novelty, due to its similarity to ZFS/btrfs implementations, but it is a timely topic, and looks like a decent implementation. 

This is mainly nit, but the comments about 10GE are somewhat incorrect. Wide area 10GE connections are common, particularly to large institutions. That said, most 10GE systems still cannot sustain 1.2 GB/s to or from local disk; there is just not sufficient spindle capacity. 

I'd really like to see a comparison to ZFS send/recv. It is a production grade implementation of a similar approach to the one in this paper. 

What performance overhead does the COW tracking process incur? 

It looks like this approach adds a division between primary filesystem storage and incremental block log. How does this approach play out in practice? Is space partitioned or shared?

===========================================================================
LISA '13 Review #4C
Updated Tuesday 11 Jun 2013 8:48:51am PDT
---------------------------------------------------------------------------
Paper #4: dsync: Efficient Block-wise Synchronization of Multi-Gigabyte
Binary Data
---------------------------------------------------------------------------

Overall merit: 4. Accept
Reviewer expertise: 3. Knowledgeable

===== Paper summary =====

The paper describes the dsync system, a scheme for distributing minimal changesets for devices by tracking which blocks have been modified.

===== Comments for author =====

Overall, the paper is well written and clearly presented. I especially liked that the "Related Work" section was towards the end of the paper; it makes it easier for the reader to understand how it relates to the proposed scheme. There are a number of small issues that should be fixed, though.

Settle on a disk size that you care about. Section 1 talks about 3TB drives, and section 3.4 talks about 2TB drives. I would suggest you acknowledge that these are growing all the time and pick one for your measurements (say 4TB). Amend table 1 accordingly.

The idea of tracking changes to devices is a clever one, but you should mention troublesome devices, namely high turnover devices (such as used for /tmp). For these, might it just be quicker to read the whole device than seeking around?

In section 2, the paragraph about network speeds was clumsy and unconvincing. Disk is growing faster than network speed, long distance network speeds are still slowish, and nowadays the issue of remote copies for disaster recovery and business continuity is more urgent; these ideas seem more to the point.

The mechanism in 3.5 for exporting the block changed vector is clever, but isn't there a race condition? I would have thought you needed semantics of "read the bit vector and zero teh bit vector atomically". And yes, it either needs to be preserved across boots OR there needs to be an indicator that information has been lost.

The usefulness of your dsync utterly depends on the statistics on block-level changes; you need to evaluate some real life traces as well as your current benchmarks.

This would also help evaluate whether you want to do checksum filtering of the changed blocks (that is, how often is a block rewritten with teh same contents?).

I was expecting a graph (like figures 2-7) that showed the relationship between chunk size (what is represented by a bit) and performance/effectiveness.
