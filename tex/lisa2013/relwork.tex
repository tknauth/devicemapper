\section{Related work}
\label{sec:relwork}

\begin{figure}[t]
  \includegraphics{figs/synctime-bar-hdd}
  \caption{Synchronization times for realistic block-level update patterns on HDDs.
Lower is better.
The results for the remaining days 3-6 are identical to the first.}
  \label{fig:synctime-bar-hdd}
\end{figure}

%% Live migration over wide area network \cite{bradford2007live}

%% Active and passive replication;
%% examples of active replication schemes;
%% examples of passive replication schemes;
%% rdb;
%% live migration of virtual machines/storage
{\tt lvmsync}~\cite{lvmsync} is a tool with identical intentions as {\sysname},
but less generic than {\sysname}.
While {\sysname} operates on arbitrary block devices,
{\tt lvmsync} only works for partitions managed by the logical volume manager~(LVM).
{\tt lvmsync} extracts the modifications that happened to an LVM partition since the last snapshot.
To provide snapshots, LVM has to keep track of the modified blocks, which is stored as meta-data.
{\tt lvmsync} uses this meta-data to identify and extract the changed blocks.

File systems, such as ZFS, and only recently btrfs,
also support snapshots and differential backups.
In ZFS, differential backups are performed using 
the ZFS \emph{send} and \emph{receive} operations.
The delta between two snapshots can be extracted and merged again with another snapshot copy,
e.g., at a remote backup machine.
Only users of ZFS, however, can enjoy those features.
For btrfs, there exists a patch to extract differences between two snapshot states~\cite{btrfs-send-receive}.
This feature is, however, still considered experimental.
Besides the file system, support for block tracking can be implemented higher up still in the software stack.
VMware ESX, since version 4, is one example which supports block tracking at the application layer.
In VMware ESX server the feature is called \emph{changed block tracking}.
Implementing support for efficient, differential backups at the block-device level, like {\sysname} does,
is more general, because it works regardless of the file system and application running on top.

If updates must be replicated more timely to reduce the inconsistency window,
the distributed replicated block device~(DRBD) synchronously replicates data at the block level.
All writes to the primary block device are mirrored to a second, standby copy.
If the primary block device becomes unavailable,
the standby copy takes over.
In single primary mode, only the primary receives updates which is required by file systems lacking concurrent access semantics.
Non-concurrent file systems assume exclusive ownership of the underlying device and single primary mode is the only viable DRBD configuration in this case.
However, DRBD also supports dual-primary configurations, where both copies receive updates.
A dual-primary setup requires a concurrency-aware file system, such as GFS or OCFS, to maintain consistency.
DRBD is part of Linux since kernel version 2.6.33.

\begin{figure}[t]
  \includegraphics{figs/synctime-bar-ssd}
  \caption{Synchronization times for realistic block-level update patterns on SSDs.
Lower is better.
The results for the remaining days 3-6 are identical to the first.}
  \label{fig:synctime-bar-ssd}
\end{figure}

There also exists work to improve the efficiency of synchronization tools.
For example, \citet{rasch2003place} proposed for \verb+rsync+ to perform in-place updates.
While their intention was to improve \verb+rsync+ performance on resource-constraint mobile devices,
their approach also helps with large data sets on regular hardware.
Instead of creating an out-of-place copy and atomically swapping this into place at the end of the transfer,
the patch performs in-place updates.
Since their original patch, in-place updates have been integrated into regular \verb+rsync+.

A more recent proposal tackles the problem of \emph{page cache pollution}~\cite{rsync-fadvise-patch}.
During the backup process many files and related meta-data are read.
To improve system performance, Linux uses a page cache, which keeps recently accessed files in main memory.
By reading large amounts of data,
which will likely \emph{not} be accessed again in the near future,
the pages cached on behalf of other processes
must be evicted.
The above mentioned patch reduces cache pollution to some extent.
The operating system is advised, via the \verb+fadvise+ system call,
that pages, accessed as part of the \verb+rsync+ invocation,
can be evicted immediately from the cache.
Flagging pages explicitly for eviction,
helps to keep the working sets of other processes in memory.
%%% Is it worth making note of uncacheable memory as an alternate/supporting approach?

Effective buffer cache management was previously discussed, for example,
by~\citet{burnett2002exploiting} and \citet{plonka2007application}.
\citet{burnett2002exploiting} reverse engineered the cache replacement algorithm used in the operating system.
They used knowledge of the replacement algorithm at the application level,
here a web server,
to change the order in which concurrent requests are processed.
As a result, the average response time decreases and throughput increases.
\citet{plonka2007application} adapted their network monitoring application to give the operating system hints about which blocks can be evicted from the buffer cache.
Because the application has ultimate knowledge about access and usage patterns,
the performance with application-level hints to the OS is superior.
Both works agree with us on the sometimes adverse effects of the default caching strategy.
Though the strategy certainly improves performance in the average case,
subjecting the system to extreme workloads,
will reveal that sometimes the default is ill suited.


%% unison: another tool for mirroring directory trees
%% http://www.google.de/patents?hl=en&lr=&vid=USPAT6985915&id=N2l4AAAAEBAJ&oi=fnd&dq=synchronization+block+level+backup&printsec=abstract#v=onepage&q=synchronization%20block%20level%20backup&f=false related US patent
%% 
