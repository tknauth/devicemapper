\section{Evaluation}
\label{sec:eval}

%% The evaluation focuses on the following aspects:
%% \begin{itemize}
%% \item What is the impact of synchronizing multiple gigabytes of state on workloads running in parallel on the source and destination server?
%% \item How much does synchronization time improve for a typical usage scenario?
%% \item How does {\sysname} decrease resource utilization (CPU, disk I/O bandwidth, network bandwidth) compared to a traditional tool like, e.g., rsync?
%% \end{itemize}

The evaluation concentrates on the question of how much the synchronization time decreases by 
knowing the modified blocks in advance.
We compare {\sysname} with four other synchronization methods:
(a)~{\tt copy}, (b)~\verb+rsync+, (c)~{\tt blockmd5sync}, (d)~ZFS send/receive.
{\tt blockmd5sync} is our custom implementation of a lightweight \verb+rsync+.
The following sections cover each tool/method in more detail.

\subsection{Synchronization tools}

\subsubsection{scp/nc}

\verb+scp+, short for secure copy, copies entire files or directories over the network.
The byte stream is encrypted, hence \emph{secure} copy, putting additional load on the endpoint CPUs of the transfer.
Compression is optional and disabled for our evaluation.
The maximum throughput over a single encrypted stream we achieved with our benchmark systems was 55~MB/s 
using the (default)  aes128-ctr cipher.
%{\color{red}(default aes128-ctr cipher)}.
%{\color{red}(default aes128-ctr cipher)}.
This is half of the maximum throughput of a 1 gigabit Ethernet adapter.
The achievable network throughput 
for our evaluation
is CPU-bound by the single threaded SSH implementation.
With a patched version of ssh~\footnote{\url{http://www.psc.edu/index.php/hpn-ssh}} encryption can be parallelized for some ciphers, e.g., aes128-ctr.
The application level throughput of this parallelized version varies between 70 to 90~MB/s.
Switching to a different cipher, for example, aes128-cbc, gives an average throughput of 100~MB/s.

To transfer data unencrypted,
\verb+nc+, short for netcat,
can be used as an alternative to ssh.
%% With netcat we were able to saturate the gigabit link.
%% Due to space limitation we do not report on measurements with unencrypted network traffic.
Instead of netcat,
the patched version of \verb+ssh+ also supports unencrypted data transfers.
Encryption is dropped after the initial secure handshake,
giving us a clearer picture of the CPU requirements for each workload.
The throughput for an unencrypted ssh transfer was 100~MB/s on our benchmark systems.
We note, however, that whilst useful for evaluation purposes, disabling encryption in a production environment 
is unlikely to be acceptable and has other practical disadvantages, for example,
%may be a bad idea, as 
encryption also helps to detect (non-malicious) in-flight data corruption.

\subsubsection{rsync}

\verb+rsync+ is used to synchronize two directory trees.
The source and destination can be remote in which case data is transferred over the network.
Network transfers are encrypted by default because \verb+rsync+ utilizes secure shell (\verb+ssh+) access to establish a connection between the source and destination.
If encryption is undesirable,
the secure shell can be replaced by its unencrypted sibling, \verb+rsh+, although we again note that this is unlikely to be acceptable for production usage.
Instead of \verb+rsh+, we configured \verb+rsync+ to use the drop-in ssh replacement which supports unencrypted transfers.
\verb+rsync+ is smarter than \verb+scp+ because it employs several heuristics to minimize the transferred data.
For example, two files are assumed unchanged if their modification time stamp and size match.
For potentially updated files, \verb+rsync+ computes block-wise checksums at the source and destination.
In addition to block-wise checksums, the sender computes rolling checksums.
This allows \verb+rsync+ to efficiently handle shifted content, e.g., a line deleted from a configuration file.
However, for binary files this creates a huge computational overhead.
Only if the checksums for a block are different is that block transferred to the destination.
For an in-depth description of the algorithm please refer to the work of~\citet{tridgell1996rsync}.
While \verb+rsync+ minimizes the amount of data sent over the network,
computing checksums for large files poses a high CPU overhead.
Also note, that the checksum computation takes place at both the source \emph{and} destination,
although only the source computes rolling checksums.

%%% Ummm...  that's not strictly true.  For the rolling checksums, 
%%% the destination computes rolling checksums, but for each /block/.
%%% The source computes the rolling checksum for each /byte/ in 
%%% the input -- it has to do this to allow detection of moved
%%% blocks (e.g. lines added/deleted) as you note in the next subsubsection.
%%% For blocks where the rolling checksum is the same, the
%%% disambiguation checksum is calculated by both sides.
%%%    [[ See Tridge's thesis, pp53 ]]

\subsubsection{Blockwise checksums ({\tt blockmd5sync})}

\verb+rsync+'s performance is limited by its use of rolling checksums at the sender.
If we discard the requirement to detect shifted content,
a much simpler and faster approach to checksum-based synchronization becomes feasible.
We compute checksums only for non-overlapping 4KiB blocks at the sender and receiver.
If the checksums for block $B_i$ do not match, this block is transferred.
For an input size of $N$ bytes, $\lceil{N/B}\rceil$ checksums are computed at the source and target,
where $B$ is the block size, e.g., 4~kilobytes.
The functionality is implemented as a mix of Python and bash scripts,
interleaving the checksum computation with the transmission of updated blocks.
We do not claim our implementation is the most efficient,
but the performance advantages over \verb+rsync+ will become apparent in the evaluation section.

% I think it's worth pointing out that this system is also directly comparable to dsync; 
% it is block-based for starters, and in taking a simple but obvious approach to 
% synchronisation by transferring the entire file, it actually provides a worst-case bound
% on dsync performance.


\subsubsection{ZFS}

The file system ZFS was originally developed by Sun for their Solaris operating system.
%% Citation??
It combines many advanced features, such as logical volume management, which are commonly handled by different tools 
in a traditional Linux environment.
Among these advanced features is snapshot support;
along with extracting the difference between two snapshots.
We include ZFS in our comparison because it offers the same functionality as {\sysname} albeit implemented at a different abstraction level.
Working at the file system layer allows access to information unavailable at the block level.
For example, updates to paths for temporary data, such as \verb+/tmp+, may be ignored during synchronization.
On the other hand, advanced file systems, e.g., like ZFS, may not be available on the target platform and {\sysname} may be a viable alternative.
As ZFS relies on a copy-on-write mechanism to track changes between snapshots,
the resulting disk space overhead must also be considered.

Because of its appealing features,
ZFS has been ported to systems other than Solaris (\cite{LinuxZFS}, \cite{FreeBSD-ZFS}.
We use version 0.6.1 of the ZFS port available from \url{http://zfsonlinux.org} packaged for Ubuntu.
It supports the necessary \emph{send} and \emph{receive} operations to extract and merge snapshot deltas, respectively.
While ZFS is available on platforms other than Solaris,
the port's maturity and reliability may discourage administrators from adopting it.
We can only add anecdotal evidence to this, by reporting one failed benchmark run due to issues within the ZFS kernel module.

\subsubsection{dsync}

Our synchronization tool, {\sysname}, differs from \verb+rsync+ in two main aspects:
\begin{enumerate}[(a)] %label=\alph*]
\item {\sysname} is file-system agnostic because it operates on the block-level.
While being file-system agnostic makes {\sysname} more versatile,
exactly because it requires no file-system specific knowledge,
it also constrains the operation of {\sysname} at the same time.
All the file-system level meta-data, e.g., modification time stamps,
which are available to tools like, e.g., \verb+rsync+,
are unavailable to {\sysname}.
{\sysname} implicitly assumes that the synchronization target is older than the source.


\item Instead of computing block-level checksums at the time of synchronization,
{\sysname} tracks the per-block modification status at runtime.
This obviates the need for checksum calculation between two subsequent synchronizations.
\end{enumerate}

\noindent %
In addition to the kernel extensions, we implemented two userspace programs:
One to extract modified blocks based on the tracked information, called \verb+dmextract+.
Extracting modified blocks is done at the synchronization source.
The equivalent tool, which is run at the synchronization target, is called \verb+dmmerge+.
\verb+dmmerge+ reads a stream consisting of block numbers interleaved with block data.
The stream is merged with the target block device.
The actual network transfer is handled either by \verb+ssh+, if encryption is required,
or \verb+nc+, if encryption is unnecessary.

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{l|l|l}
%%                    & unencrypted & encrypted \\
%%       \hline
%%        copy        & netcat & scp \\
%%        rsync       & rsh    & ssh \\
%%        {\sysname}  & netcat & ssh \\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Linux tool used for unencrypted and encrypted network transfer.}
%%   \label{tab:transfer-matrix}
%% \end{table}

%% \begin{figure}
%%   \includegraphics{figs/synctime-vs-size}
%%   \caption{Synchronization time for three different synchronization techniques.
%% Lower is better. {\sysname} is the best in all cases.
%% The gap widens with increasing data set sizes.}
%%   \label{fig:rsync-time}
%% \end{figure}

\subsection{Setup}

%%%% Need to include RAM size in test machine specifications
%%%% in order to eliminate buffering effects from consideration
%%%% Linux will use as much RAM as it can to buffer.
%%%% Similarly, it's worth mentioning how the disks were connected 
%%%% (SATA II/III) and whether the network card was PCIe 
%%%% Most of these are unlikely to be limiting or enchancing factors 
%%%% but it won't hurt to be complete with respect to specification.

Our benchmark setup consisted of two machines: one sender and one receiver.
The two machines had slightly different processor and memory configurations.
While the sender was equipped with a 6-core AMD Phenom II processor and 12~GiB RAM,
the receiver only had a 4-core Phenom II processor and 8~GiB RAM.
Both units had a 2~TB spinning disk (Samsung HD204UI) as well as a 128~GB SSD (Intel SSDSC2CT12).
Both disks were attached via the Serial ATA~(SATA) revision 2.
The spinning disk had a 300~GB ``benchmark'' partition at an outer zone for maximum sequential performance.
Except for the ZFS experiments, we formatted the benchmark partition with an ext3 file system.
All benchmarks started and ended with a cold buffer cache.
We flushed the buffer cache before each run and ensured that all cached writes are flushed to disk before declaring the run finished.
The machines had a PCIe Gigabit Ethernet card which was connected to a gigabit switch.
We ran a recent version of Ubuntu (12.04) with a 3.2 kernel.
Unless otherwise noted, each data point is the mean of three runs.
The synchronized data set consisted of a single file of the appropriate size, e.g., 16~GiB,
filled with random data.
If a tool interfaced with a block device,
we created a loopback device on top of the single file.

\subsection{Benchmarks}

We used two types of benchmarks,
synthetic and realistic,
to evaluate {\sysname}'s performance and compare it with its competitors.
While the synthetic benchmarks allow us to investigate worst case behavior,
the realistic benchmarks show expected performance for more typical scenarios.

\subsubsection{Random modifications}

In our synthetic benchmark, we randomly modified varying percentages of data blocks.
Each block had the same probability to be modified.
Random modification is a worst case scenario because there is little spatial locality.
Real world applications, on the other hand, usually exhibit spatial locality with respect to the data they read and write.
Random read/write accesses decrease the effectiveness of data prefetching and write coalescing.
Because conventional spinning hard disks have a tight limit on the number of input/output operations per second~(IOPS),
random update patterns are ill suited for them.

\subsubsection{RUBiS}

A second benchmark measures the time to synchronize virtual machine images.
The tracked VM ran the RUBiS~\cite{rubis} server components,
while another machine ran the client emulator.
RUBiS is a web application modeled after \url{eBay.com}.
The web application, written in PHP, consists of a web server,
and a database backend.
Users put items up for sale, bid for existing items or just browse the catalog.
Modifications to the virtual machine image resulted, for example,
from updates to the RUBiS database.

A single run consisted of booting the instance,
subjecting it to 15 minutes of simulated client traffic,
and shutting the instance down.
During the entire run,
we recorded all block level updates to the virtual machine image.
The modified block numbers were the input to the second stage of the experiment.
% where we timed how long each synchronization method takes based on the observed update pattern.
The second stage used the recorded block modification pattern while measuring the synchronization time.
Splitting the experiment into two phases allows us to perform and repeat them independently.

\subsubsection{Microsoft Research Traces}

\citet{narayanan2008write} collected and published block level traces for a variety of servers and services at Microsoft Research~\footnote{available at \url{ftp://ftp.research.microsoft.com/pub/austind/MSRC-io-traces/}}.
The traces capture the block level operations of, among others, print, login, and file servers.
Out of the available traces we randomly picked the print server trace.
Because the print server's non-system volume was several hundred gigabytes in size,
we split the volume into non-overlapping, 32~GiB-sized ranges.
Each operation in the original trace was assigned to exactly one range,
depending on the operation's offset.
Further analysis showed that the first range, i.e., the first 32~GiB of the original volume,
had the highest number of write operations, close to 1.1~million;
more than double of the second ``busiest'' range.

In addition to splitting the original trace along the space axis,
we also split it along the time axis.
The trace covers over a period of seven days,
which we split into 24 hour periods.

To summarize: for our analysis we use the block modification pattern for the first 32~GiB of the print server's data volume.
The seven day trace is further divided into seven 24 hour periods.
The relative number of modified blocks is between 1\% and 2\% percent for each period.

\subsection{Results}

\subsubsection{Random modifications}

\begin{figure*}
  \begin{tabular}{cc}
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/rsync-1373558022-1375372816-synctime-vs-size-hdd}
      % \caption{.}
      % \label{fig:synctime-vs-size-hdd}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/rsync-1373558022-1375372816-synctime-vs-size-ssd}
      % \caption{.}
      % \label{fig:synctime-vs-size-ssd}
    \end{minipage}
    \\
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/rsync-1373558022-1375372816-synctime-vs-size-hdd-without-rsync}
      \caption{Synchronization time for five different synchronization techniques.
Lower is better. Data on the source and target was stored on HDD.}
      \label{fig:synctime-vs-size-hdd}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/rsync-1373558022-1375372816-synctime-vs-size-ssd-without-rsync}
      \caption{Synchronization time for five different synchronization techniques.
Lower is better. Data on the source and target was stored on SSD.}
      \label{fig:synctime-vs-size-ssd}
    \end{minipage}
  \end{tabular}
\end{figure*}


\begin{figure*}
  \begin{tabular}{cc}
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/cpu-utilization}
      \caption{CPU utilization for a sample run of three synchronization tools.
100\% means all cores are busy.
%% Measurement is for the entire system including all processes/threads.
%% Includes time spent in user and kernel mode as well as waiting for I/O.
      }
      \label{fig:cpu-utilization}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/net-tx}
      \caption{Network transmit traffic on the sender side measured for the entire system.
rsync and dsync transmit about the same amount of data in total, although the effective throughput of {\tt rsync} is much lower.
%% copy transmits much more data than dsync and achieves throughput rates of 80~MB/s.
      }
      \label{fig:net-tx}
    \end{minipage}
  \end{tabular}
\end{figure*}

%% Being fussy, but using `copy' in main text makes it difficult to differentiate
%% between `copy' the method and `copy' the activity.  Similar for blockmd5sync.
%% ZFS doesn't have this problem because it's capitalised.  Perhaps {\sc copy} or {\tt copy}?
We start the evaluation by looking at how the data set size affects the synchronization time.
The size varies between 1 and 32~GiB for which we randomly modified 10\% of the blocks.
The first set of results is shown in Figure~\ref{fig:synctime-vs-size-hdd}.
First of all,
we observe that the synchronization time increases linearly with the data set size;
irrespective of the synchronization method.
Second, \verb+rsync+ takes longest to synchronize, followed by {\tt blockmd5sync} on HDD and {\tt copy} on SSD.
{\tt copy}, ZFS, and {\sysname} are fastest on HDD and show similar performance.
On SSD, {\sysname} and {\tt blockmd5sync} are fastest, with ZFS being faster than {\tt copy}, but not as fast as {\sysname} and {\tt blockmd5sync}.
With larger data sizes, the performance difference is more markedly:
for example, at 32~GiB {\sysname}, {\tt copy}, and ZFS perform almost identically (on HDD),
while \verb+rsync+ takes almost five times as long (420 vs. 2000 seconds).
To our surprise, copying the entire state is sometimes as fast as or even slightly faster than extracting and merging the differences.
Again at 32~GiB, for example, {\tt copy} takes about 400 seconds,
compared with 400 seconds for {\sysname} and 420 seconds for ZFS.

We concluded that the random I/O operations were inhibiting {\sysname} performance.  
% to really shine.
Hence, we performed a second set of benchmarks where we used SSDs instead of HDDs.
The results are shown in Figure~\ref{fig:synctime-vs-size-ssd}.
While the increased random I/O performance of SSDs does not matter for \verb+rsync+,
its synchronization time is identical to the HDD benchmark,
SSDs enable all other methods to finish faster.
{\sysname}'s time to synchronize 32~GiB drops from 400~s on HDD to only 220~s on SSD.

%% Given the synchronization times and knowing the input data size,
%% we can calculate the throughput.
%% Note, however, that throughput is defined in terms of how long it takes to processes a certain amount of input data.
%% In the case of \verb+rsync+ and copy the input data is read completely exactly once.
%% So the definition of throughput is intuitively valid.
%% For {\sysname} though, only the modified parts are read and transmitted.
%% That is, the size of processed data is smaller than the total size.
%% In our synthetic benchmark,
%% we modified 10\% of the blocks,
%% hence the processed data size is $1/10$th of the input data.
%% In this light,
%% the throughput based on the processed data is $1/10$th of the throughput shown for {\sysname} in Figure~\ref{fig:tput-vs-size-nocache-nossd}.
%% For example, at 32~GB the throughput for {\sysname} based on the input size is ca. 40~MB/s.
%% Based on the data actually transmitted over the network,
%% the throughput is only 4~MB/s, however.

%% A plain copy achieves around 35~MB/s.
%% This is 1/3 of the theoretical bandwidth of gigabit Ethernet.
%% The limiting factor is CPU speed.
%% Our CPU cannot encrypt more than 35~MB of data per second.
%% Switching to a cheaper encryption, e.g., arcfour,
%% or abandoning encryption altogether would increase the throughput.
%% \verb+rsync+'s throughput is 20~MB/s for data sizes up to 4~GB,
%% but drops to 10~MB/s and less for larger data sets.
%% \verb+rsync+ is also CPU-bound due to its checksum computation.
%% Throughput for \verb+dsync+ as defined by the input data size is equivalent to copy's throughput.
%% If throughput is calculated based on the data's transfer size,
%% {\sysname}'s throughput with ca. 4~MB/s,
%% is worse than for \verb+rsync+.

Intrigued by the trade-off between hard disk and solid state drives,
we measured the read and write rate of our drives outside the context of {\sysname}.
When extracting or merging modified blocks they are processed in increasing order by their block number.
We noticed that the read/write rate increased by up to 10x when processing a \emph{sorted} randomly generated sequence of block numbers compared to the same \emph{unsorted} sequence.
For a random but sorted sequence of blocks our HDD achieves a read rate of 12~MB/s and a write rate of 7~MB/s.
The SSD reads data twice as fast at 25~MB/s and writes data more than 15x as fast at 118~MB/s.
This explains why, if HDDs are involved,
{\tt copy} finishes faster than {\sysname} although {\tt copy}'s transfer volume is 9x that of {\sysname}:
sequentially going through the data on HDD is much faster than selectively reading and writing only changed blocks.

% Note that {\sysname} reads/writes blocks in increasing order.
% Entirely random read/write access on HDD would be even slower than the stated 12~MB/s for reading and 4-5~MB/s for writing.

% \begin{table}
%   \begin{center}
%     \begin{tabular}{l|l|l}
%                    & read [MB/s] & write [MB/s] \\
%       \hline
%        HDD & 12 & 7 \\
%        SSD & 25 & 118 \\
%     \end{tabular}
%   \end{center}
%   \caption{Read and write rate for random, sorted block numbers.}
%   \label{tab:read-write-rate}
% \end{table}

% Because our synthetic benchmark modifies blocks at random,
% there is little spatial locality.
% However, spatial locality is important to get good I/O performance from spinning media because only sequential access is fast.
% Additional measurements show that the sender reads the non-contiguous data blocks from disk at 12~MB/s.
% The receiver writes the same non-contiguous data blocks at 4-5~MB/s.

%% After identifying the bottleneck,
%% we performed a second benchmark run,
%% where all data was stored on a solid state disk.

To better highlight the differences between the methods,
we also present CPU and network traffic traces for three of the five methods.
Figure~\ref{fig:cpu-utilization} shows the CPU utilization while Figure~\ref{fig:net-tx} shows the outgoing network traffic at the sender.
The trace was collected at the sender while synchronizing 32~GiB from/to SSD.
The CPU utilization includes the time spent in kernel and user space, as well as waiting for I/O.
We observe that \verb+rsync+ is CPU-bound by its single-threaded rolling checksum computation.
Up to $t=500$ the \verb+rsync+ sender process is idle,
while one core on the receiver-side computes checksums (not visible in the graph).
During \verb+rsync+'s second phase, one core, on our 6-core benchmark machine,
is busy computing and comparing checksums for the remaining 1400~s (23~min).
The network traffic during that time is minimal at less than 5~MB/s.
{\tt copy}'s execution profile taxes the CPU much less:
utilization oscillates between 0\% and 15\%.
On the other hand, it can be visually determined that {\tt copy} generates much more traffic volume than either \verb+rsync+ or {\sysname}.
{\tt copy} generates about 90~MB/s of network traffic on average.
{\sysname}'s execution profile uses double the CPU power of {\tt copy},
but only incurs a fraction of the network traffic.
{\sysname}'s network throughput is limited by the random read-rate at the sender side.

%% Suggest:  22.5k random 4k reads~\cite{intel330ssd-specification}, about 90MiB/s,
Even though the SSD's specification promises 22.5k random 4~KiB reads~\cite{intel330ssd-specification},
about 90~MiB/s,
we are only able to read at a sustained rate of 20~MB/s at the application layer.
Adding a loopback device to the configuration, reduces the application layer read throughput by about another 5~MB/s.
This explains why {\sysname}'s sender transmits at 17~MB/s.
In this particular scenario {\sysname}'s performance is read-limited.
Anything that would help with reading the modified blocks from disk faster,
would decrease the synchronization time even further.

Until now we kept the modification ratio fixed at 10\%,
which seemed like a reasonable change rate.
Next we explored the effect of varying the percentage of modified blocks.
The data size was fixed at 8~GiB and we randomly modified 10\%, 50\%, and 90\% percent of the blocks.
%%%% Question:  Did you track which blocks were modified, i.e. could a block be modified twice,
%%%% or did you deliberately remove a block from the random pool once it had been modified once?
%%%% The reason for detailing this is that in the former case, as the percentage increases, 
%%%% it's less likely that you actually changed 90% of the blocks.  
Figure~\ref{fig:synctime-vs-randomupdates-hdd} and \ref{fig:synctime-vs-randomupdates-ssd} shows the timings for spinning and solid-state disks.
On HDD, interestingly, even though the amount of data sent across the network increases,
the net synchronization time stays almost constant for ZFS and {\tt blockmd5sync};
it even \emph{decreases} for {\sysname}.
Conversely, on SSD, synchronization takes longer with a larger number of modified blocks across all shown methods;
although only minimally so for ZFS.
We believe the increase for {\sysname} and {\tt blockmd5sync} is due to a higher number of block-level re-writes.
Updating a block of flash memory is expensive and often done in units larger than 4~KiB~\cite{cornwell2012anatomy}.
ZFS is not affected by this phenomenon, as ZFS employs a copy-on-write strategy which turns random into sequential writes.
%% see, for example, http://www.slideshare.net/jarodwang/zfs-the-last-word-in-filesystems

%% The CPU utilization of copy is more erratic than both \verb+rsync+'s and dsync's.
%% The performance of copy is bound by the sequential disk read speed.
%% Looking at the network traffic, Figure~\ref{fig:net-tx},
%% it is evident that copy uses a lot more network bandwidth than either {\sysname} or rsync.
%% At ca. 10~MB/s {\sysname}'s network throughput is only 1/10th of the theoretical maximum.
%% The random disk I/O is the limiting factor for {\sysname}.
%% For workloads with higher spatial locality the effective throughput of dsync is higher.

%% map(lambda x : x/float(32*1024*1024*1024/4096), [102111,145959,74369,80595,102266,88387,82946])
%% [0.012172579765319824, 0.01739966869354248, 0.0088654756546020508, 0.0096076726913452148, 0.012191057205200195, 0.010536551475524902, 0.0098879337310791016]

%% \begin{tabular}{rr}
%% \# updates & \% ,& day \\
%% \hline\\
%% 102111 & prn\_1\_00.csv \\
%%  145959 & prn\_1\_01.csv \\
%%   74369 & prn\_1\_02.csv \\
%%   80595 & prn\_1\_03.csv \\
%%  102266 & prn\_1\_04.csv \\
%%   88387 & prn\_1\_05.csv \\
%%   82946 & prn\_1\_06.csv \\
%% \end{tabular}
%% }

\begin{figure}[t]
  \includegraphics{figs/rsync-1375124511-synctime-vs-randomupdates-hdd}
  \caption{For comparison, {\tt rsync} synchronizes the same data set 6, 21, and 41 minutes, respectively.
{\tt copy} took between 1.5 and 2~minutes.}
  \label{fig:synctime-vs-randomupdates-hdd}
\end{figure}

\subsubsection{RUBiS results}

%% Random modifications represent the worst case for every method which reads and writes only modified blocks.
%% Real-world update patterns are not random, however, but show spatial locality.
%% To create more realistic modification traces,
%% we recorded block-level modifications to a virtual machine disk.

We argued earlier,
that a purely synthetic workload of random block modifications artificially constrains the performance of {\sysname}.
Although we already observed a 5x improvement in total synchronization time over \verb+rsync+,
the gain over {\tt copy} was less impressive.
To highlight the difference in spatial locality between the synthetic and RUBiS benchmark,
we plotted the number of consecutive modified blocks for each;
this is illustrated in Figure~\ref{fig:consecutive-blocks-cdf}.

We observe that 80\% of the modifications involve only a single block (36k blocks at $x=1$ in Figure~\ref{fig:consecutive-blocks-cdf}).
In comparison, there are no single blocks for the RUBiS benchmark.
Every modification involves at least two consecutive blocks (1k blocks at $x=2$).
At the other end of the spectrum,
the longest run of consecutively modified blocks is 639 for the RUBiS benchmarks.
Randomly updated blocks rarely yield more than 5 consecutively modified blocks.
For the RUBiS benchmark, updates of 5 consecutive blocks happen most often:
the total number of modified blocks jumps from 2k to 15k moving from 4 to 5 consecutively modified blocks.

%% The synthetic benchmark has at most five consecutive modified blocks,
%% compared to 639 modified blocks in a row for the RUBiS benchmark.
%% Modifications of 4 consecutive blocks were most common for the realistic benchmark:
%% about 60\% of the modifications involved four consecutive blocks.
%% 16 kilobyte happens to be the size of the XXX data structure,
%% which is likely responsible for the high percentage of 4 consecutive blocks.

\begin{figure}[t]
  \includegraphics{figs/rsync-1375124511-synctime-vs-randomupdates-ssd}
  \caption{Varying the percentage of modified blocks for an 8~GiB file/device.
For comparison, {\tt rsync} synchronizes the same data set in 5, 21, and 41 minutes, respectively.
A plain copy consistently took 1.5 minutes.}
  \label{fig:synctime-vs-randomupdates-ssd}
\end{figure}


\begin{figure}
  \includegraphics{figs/consecutive-blocks-frequency-vs-totalblocks}
  \caption{Difference in spatial locality between a synthetic and realistic benchmark run.
  In both cases 45k blocks are modified.
  For the synthetic benchmark 80\% are isolated individual blocks (36k at x=1).
  The realistic benchmark shows a higher degree of spatial locality, as observed, for example,
  by the jump from 2.5k (x=3) to 15k (x=4) blocks.}
  \label{fig:consecutive-blocks-cdf}
\end{figure}

Now that we have highlighted the spatial distribution of updates,
Figure~\ref{fig:synctime-vs-blocks-rubis-hdd} illustrates the results for our RUBiS workload.
We present numbers for the HDD case only because this workload is less constrained by the number of I/O operations per second.
%% This time, we present numbers only for the HDD case.
%% The numbers for SSDs are almost identical and show the same trends.
The number of modified blocks was never the same between those 20 runs.
Instead, the number varies between 659 and 3813 blocks.
This can be explained by the randomness inherent in each RUBiS benchmark invocation.
The type and frequency of different actions,
e.g., buying an item or browsing the catalog,
is determined by chance.
Actions that modify the database increase the modified block count.

\begin{figure}[t]
  \includegraphics{figs/synctime-vs-blocks-rubis}
  \caption{Synchronization time for (a) {\tt copy}, (b) {\tt rsyn}c, and (c) {\tt dsync}.
Block modifications according to the RUBiS workload.}
  \label{fig:synctime-vs-blocks-rubis-hdd}
\end{figure}

%% Looking at each synchronization tool in isolation reveals little difference in synchronization time based on the number of modified blocks.
The synchronization time shows little variation between runs of the same method.
{\tt copy} transfers the entire 11~GiB of data irrespective of actual modifications.
There should, in fact, be no difference based on the number of modifications.
\verb+rsync+'s execution time is dominated by checksum calculations.
%% Calculating checksums is also independent of the modified block count.
\verb+dsync+, however, transfers only modified blocks and should show variations.
The relationship between modified block count and synchronization time is just not discernible in Figure~\ref{fig:synctime-vs-blocks-rubis-hdd}.
Alternatively, we calculated the correlation coefficient for {\sysname} which is 0.54.
This suggests a positive correlation between the number of modified blocks and synchronization time.
The correlation is not perfect because factors other than the raw modified block count affect the synchronization time,
e.g., the spatial locality of updates.
%% Spacial locality of the modified blocks differs between the individual RUBiS runs.
%% Hence, the correlation between synchronization duration and modified block count for {\sysname} is not perfect.

The performance in absolute numbers is as follows:
\verb+rsync+, which is slowest, takes around 320 seconds to synchronize the virtual machine disk.
The runner up, {\tt copy}, takes 200 seconds.
The clear winner,
with an average synchronization time of about 3 seconds,
is {\sysname}.
That is a 66x improvement over {\tt copy} and more than 100x faster than \verb+rsync+.
{\sysname} reduces the network traffic to a minimum, like \verb+rsync+,
while being 100x faster.
Table~\ref{tab:performance-summary} summarizes these results.

%% \begin{figure*}
%%   \begin{tabular}{cc}
%%     \begin{minipage}[t]{\columnwidth}
%%       \includegraphics{figs/tput-vs-size-hdd}
%%       \caption{Synchronization time for (a) copy, (b) rsync, and (c) dsync.
%% Block modifications according to the RUBiS workload.
%% Backing storage is a spinning disk (HDD).}
%%       \label{fig:synctime-vs-size-rubis-hdd}
%%     \end{minipage}
%%     &
%%     \begin{minipage}[t]{\columnwidth}
%%       \includegraphics{figs/tput-vs-size-ssd}
%%       \caption{Synchronization time for (a) copy, (b) rsync, and (c) dsync.
%% Block modifications according to the RUBiS workload.
%% Backing storage is a solid-state disk (SDD).}
%%       \label{fig:synctime-vs-size-rubis-ssd}
%%     \end{minipage}
%%   \end{tabular}
%% \end{figure*}

%% \subsection{Impact on workload}

%% \begin{figure}
%%   \includegraphics{figs/lorem}
%%   \caption{Impact of (1)~rsync and (2)~{\sysname} on request throughput running on the synchronization source. Throughput drops by xx \% for rsync, but only yy \% for {\sysname}.}
%%   \label{fig:rsync-progress}
%% \end{figure}

%% Show impact of rsync and {\sysname} on workload running in parallel on the synchronization source/target.

\subsubsection{Microsoft Research Traces}

In addition to our benchmarks with synchronizing a single virtual machine disk,
we used traces from a Microsoft Research~(MSR) printer server.
The speed with which the different methods synchronize the data is identical across all days of the MSR trace.
Because of the homogeneous run times, we only plot three days out of the total seven.

\verb+rsync+ is slowest, taking more than 900 seconds (15 minutes) to synchronize 32~GiB of binary data.
The small number of updated blocks (between 1-2\%) decreases the runtime of \verb+rsync+ noticeably.
Previously, with 10\% updated blocks \verb+rsync+ took 35 minutes (cf. Figure~\ref{fig:synctime-vs-size-hdd}) for the same data size.
{\tt copy} and {\tt blockmd5sync} finish more than twice as fast as \verb+rsync+,
but are still considerably slower than either ZFS or {\sysname}.
The relative order of synchronization times does not change when we swap HDDs for SSDs (Figure~\ref{fig:synctime-bar-ssd}).
Absolute synchronization times improve for each synchronization method.
{\tt blockmd5sync} sees the largest decrease as its performance is I/O bound on our setup:
the SSD offers faster sequential read speeds than the HDD, 230~MB/s vs 130~MB/s.

\subsection{Discussion}

One interesting question to ask is if there exist cases where {\sysname} performs worse than \verb+rsync+.
For the scenarios investigated in this study {\sysname} always outperformed \verb+rsync+ by a significant margin.
In fact, we believe, that {\sysname} will always be faster than \verb+rsync+.
Our reasoning is that the operations performed by \verb+rsync+ are a superset of {\sysname}'s operations.
\verb+rsync+ must read, transmit, and merge all the updated blocks; as does {\sysname}.
However, to determine the updated blocks \verb+rsync+ must read \emph{every} block and compute its checksum at the source \emph{and} destination.
As illustrated and mentioned in the capture for Figure~\ref{fig:cpu-utilization}, the computational overhead varies with the number of modified blocks.
For identical input sizes, the execution time of \verb+rsync+ grows with the number of updated blocks.

The speed at which {\sysname} synchronizes depends to a large degree on the spatial distribution of the modified blocks.
This is most visible in Figures~\ref{fig:synctime-vs-randomupdates-hdd}.
Even though the data volume increases by 5x, going from 10\% randomly modified blocks to 50\%,
the synchronization takes \emph{less} time.
For the scenarios evaluated in this paper,
a simple copy typically (cf. Figure~\ref{fig:synctime-vs-randomupdates-hdd}, \ref{fig:synctime-vs-size-hdd}) took at least as long as {\sysname}.
While {\sysname} may not be faster than a plain copy in all scenarios,
it definitely reduces the volume of transmitted data.

Regarding the runtime overhead of maintaining the bitmap used to track changed blocks,
we do not expect this to noticeably affect performance in typical use cases.
Setting a bit in memory is orders of magnitude faster than actually writing a block to disk.
