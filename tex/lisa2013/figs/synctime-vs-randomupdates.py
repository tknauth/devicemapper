import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename, filters) :
    # data points
    vals = {}
    for line in open(filename) :
        if not line.startswith('method=') : continue
        kv_pairs = line.strip().split()
        kv = {}
        for kv_pair in kv_pairs :
            (k, v) = kv_pair.split('=')
            kv[k] = v

        filters_match = True
        for (k_filter, v_filter) in filters.iteritems() :
            if not kv[k_filter] == v_filter :
                filters_match = False
                break

        if not filters_match : continue

        xs = vals.setdefault(int(kv['update_percentage']), [])
        xs.append(float(kv['duration_sec']))
    return vals


plot_order = ['dsync', 'blockmd5sync', 'zfs']
method2linestyle = {'rsync' : 'o-', 'dsync' : 's-', 'copy' : '^-', 'blockmd5sync' : 'x-', 'zfs' : 'v-'}
method2color={'rsync' : '#222222', 'dsync' : '#555555', 'copy' : '#777777', 'blockmd5sync' : '#999999', 'zfs' : '#BBBBBB'}

# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(xs, ys, filename, xlabel='', ylabel='',
                       labels='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    linestyle = [method2linestyle[k] for k in plot_order]
    color = [method2color[k] for k in plot_order]
    for (i, y) in enumerate(ys) :
        plt.plot(xs, y, linestyle[i], color=color[i], label=labels[i])

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    # ax1.set_xticks([1,2,4,8,16,32])
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(0, 3.5)
    ax1.set_xlim(0, 100)
    # ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(loc=2, numpoints=1, ncol=3)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig(filename)

filename = 'rsync-1373903151'
if sys.argv[1] :
    filename = sys.argv[1]

filename_base = os.path.basename(filename)

for disk in ['hdd', 'ssd'] :
    xs = []
    multi_plot_ys = []
    labels = []
    for method in plot_order :
        vals = parse_input(filename, {'method' : method, 'transport' : 'sshnone', 'disk' : disk})
        if not xs :
            xs = sorted(vals.keys())
        else :
            if not sorted(vals.keys()) == xs :
                print sorted(vals.keys())
                print xs
                print method
            assert (sorted(vals.keys()) == xs)
        ys = []

        for x in xs :
            y = np.mean(map(float, vals[x]))
            ys.append(y/60.)
        multi_plot_ys.append(ys)
        labels.append(method)

    plot_single_figure(xs, multi_plot_ys,
                       '%s-synctime-vs-randomupdates-%s'%(filename_base, disk),
                       xlabel='percentage randomly updated blocks',
                       ylabel='synchronization time [min]',
                       labels=labels)
