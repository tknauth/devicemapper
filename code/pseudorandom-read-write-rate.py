#!/usr/bin/env python2.7

import subprocess as sp
from  shlex import split
import time

hdd_dev = '/dev/disk/by-id/scsi-SATA_SAMSUNG_HD204UIS2H7J1CB117070-part4'
ssd_dev = '/dev/disk/by-id/scsi-SATA_INTEL_SSDSC2CT1CVMP247001T8120BGN-part1'

block_size = 4096
data_size_byte = 32 * 1024 * 1024 * 1024
blocks = data_size_byte / block_size
block_update_percentage = 10
blocks_to_update = int(blocks * block_update_percentage / 100)
sp.check_call('shuf -n %d -i0-%d |sort -n > /tmp/blocks'%(blocks_to_update, blocks-1), shell=True)

cmd = {'write' : 'python ~/devicemapper/code/dmmerge.py /mnt/32GB.dat /tmp/blocks /dev/zero ; sudo sync',
       'read' : 'python ~/devicemapper/code/dmextract.py /mnt/32GB.dat /tmp/blocks > /dev/null'}

def setup(dev) :
    sp.check_call(split('sudo mount %s /mnt'%(dev)))

def teardown(dev) :
    sp.check_call(split('sudo umount /mnt'))

for dev in [hdd_dev, ssd_dev] :
    for op in ['read', 'write'] :
        setup(dev)
        begin = time.time()
        sp.check_call(cmd[op], shell=True)
        end = time.time()
        print 'device=%s op=%s time=%.2f rate=%.2f'%(dev, op, end-begin, (data_size_byte/1024/1024*(block_update_percentage/100.))/(end-begin))
        teardown(dev)
