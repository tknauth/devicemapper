#!/bin/bash

set -x

KERNEL_SRC=/mnt/myvg-data/thomas/linux-devicemapper
UBUNTU_SRC=/mnt/myvg-data/thomas/ubuntu-precise
PROJECT_DIR=/home/students/thomas/phd/devicemapper

DIFF_FILES="drivers/md/dm-linear.c drivers/md/dm-ioctl.c"

TEST_SYSTEM_IP=192.168.2.3

cd $KERNEL_SRC
make M=drivers/md
git diff 0d7614f09c1ebdbaa1599a5aba7593f147bf96ee -- $DIFF_FILES > $PROJECT_DIR/code/patch.diff
cd -

cd $UBUNTU_SRC
# revert changes from previous run
git checkout -- drivers/md/dm-ioctl.c drivers/md/dm-linear.c
# apply patch
patch -p1 < $PROJECT_DIR/code/patch.diff
# copy patched files to build directory
cp $DIFF_FILES debian/build/build-generic/drivers/md
# build only the device mapper module
make -C debian/build/build-generic M=drivers/md
cd -

scp $UBUNTU_SRC/debian/build/build-generic/drivers/md/dm-mod.ko $TEST_SYSTEM_IP:~
ssh $TEST_SYSTEM_IP 'sudo cp dm-mod.ko /lib/modules/$(uname -r)/kernel/drivers/md'
