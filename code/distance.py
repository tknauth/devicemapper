#!/usr/bin/env python2.7

import sys
from collections import defaultdict

# Reads one number per line from standard in. Outputs a histogram of
# the difference between two consecutive lines/numbers. In our case it
# will be block numbers. This is to illustrate the spacial locality
# (or absence thereof).

prev = None
d = defaultdict(int)

for line in sys.stdin :
    cur = int(line.strip())
    if not prev :
        prev = cur
        continue

    # Block numbers must be in increasing order!
    assert cur > prev

    d[cur-prev] += 1
    prev = cur

for k in d.keys() :
    print k, d[k]
