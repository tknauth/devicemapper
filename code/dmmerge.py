#!/usr/bin/env python2.7

import os
import sys

block_size=4*1024

def merge_block(block_number, fd_dev, fd_block_data=None) :
    os.lseek(fd_dev, block_number*block_size, os.SEEK_SET)
    block = None
    if fd_block_data :
        block = os.read(fd_block_data, block_size)
    else :
        block = sys.stdin.read(block_size)
    assert block
    bytes_written = os.write(fd_dev, block)
    assert(bytes_written == block_size)

def main_files(dev_path, blocks_filename, block_data_filename) :

    fd_block_data = os.open(block_data_filename, os.O_RDONLY)
    fd_dev = os.open(dev_path, os.O_WRONLY)

    for line in open(blocks_filename) :
        block_number = int(line.strip())
        merge_block(block_number, fd_dev, fd_block_data)

    os.close(fd_dev)
    os.close(fd_block_data)

def main_stdin(dev_path) :

    fd_dev = os.open(dev_path, os.O_WRONLY)

    while True :
        block_number = sys.stdin.read(8)
        if block_number == '' : break
        block_number = int(block_number)
        merge_block(block_number, fd_dev)

    os.close(fd_dev)

# reads from stdin and merges into device passed as first argument
if __name__ == '__main__' :
    if len(sys.argv) == 2 :
        dev_path = sys.argv[1]
        main_stdin(dev_path)
        pass
    elif len(sys.argv) == 4 :
        dev_path = sys.argv[1]
        blocks_filename = sys.argv[2]
        block_data = sys.argv[3]
        main_files(dev_path, blocks_filename, block_data)
    else :
        assert False
