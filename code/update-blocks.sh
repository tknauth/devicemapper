#!/bin/bash

set -x

# Read block numbers from stdin. Update blocks on $1 with random data.
# $1 may be a device or file (must support seeking).

DEV=$1

# Store to temporary file to determine how many blocks there are.
cat > /tmp/blocknr
blocks=$(wc -l /tmp/blocknr | cut -f1 -d' ')

# Generate random data for the number of blocks to update. Store
# random data to temporary file.
dd if=/dev/zero bs=4k count=$blocks 2> /dev/null | \
    openssl enc -aes-256-cbc -pass pass:"$(dd if=/dev/urandom bs=128 count=1 2>/dev/null | base64)" -nosalt 2>/dev/null > /tmp/blockdata

# Call helper to do the actual updating
python ~/devicemapper/code/dmmerge.py $DEV /tmp/blocknr /tmp/blockdata

rm /tmp/blockdata /tmp/blocknr
