#!/usr/bin/env python2.7

import os
import sys

block_size=4*1024

def extract_block(block_number, fd_dev) :
    os.lseek(fd_dev, block_number*block_size, os.SEEK_SET)
    block = os.read(fd_dev, block_size)
    sys.stdout.write('%08d'%block_number)
    sys.stdout.write(block)

def main(dev_path, proc_file) :
    fd_dev = os.open(dev_path, os.O_RDONLY)
    for line in open(proc_file) :
        block_number = int(line.strip())
        extract_block(block_number, fd_dev)
    os.close(fd_dev)

def print_usage() :
    print 'dmextract <dev mapper name> or'
    print 'dmextract <block device> <proc file>'

if __name__ == '__main__' :

    # If there is only one argument, we assume it is the device mapper
    # name. Otherwise, first parameter is the device, second parameter
    # is the proc file.

    if len(sys.argv) == 2 :
        dev_name = sys.argv[1]
        dev_path = '/dev/mapper/%s'%(dev_name)
        proc_file = '/proc/%s'%(dev_name)
    elif len(sys.argv) == 3 :
        dev_path = sys.argv[1]
        proc_file = sys.argv[2]
    else :
        print_usage()
        sys.exit(1)

    main(dev_path, proc_file)
