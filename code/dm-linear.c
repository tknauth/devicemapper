/*
 * Copyright (C) 2001-2003 Sistina Software (UK) Limited.
 *
 * This file is released under the GPL.
 */

#include "dm.h"
#include <linux/module.h>
#include <linux/init.h>
#include <linux/blkdev.h>
#include <linux/bio.h>
#include <linux/slab.h>
#include <linux/device-mapper.h>
#include <linux/dm-ioctl.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/seq_file.h>

#define DM_MSG_PREFIX "linear"

#define MAX_DEPTH 16

struct mapped_device *find_device(struct dm_ioctl *param);

struct mapped_device {
	struct rw_semaphore io_lock;
	struct mutex suspend_lock;
	rwlock_t map_lock;
	atomic_t holders;
	atomic_t open_count;

	unsigned long flags;

	struct request_queue *queue;
	unsigned type;
	/* Protect queue and type against concurrent access. */
	struct mutex type_lock;

	struct target_type *immutable_target_type;

	struct gendisk *disk;
	char name[16];

	void *interface_ptr;

	/*
	 * A list of ios that arrived while we were suspended.
	 */
	atomic_t pending[2];
	wait_queue_head_t wait;
	struct work_struct work;
	struct bio_list deferred;
	spinlock_t deferred_lock;

	/*
	 * Processing queue (flush)
	 */
	struct workqueue_struct *wq;

	/*
	 * The current mapping.
	 */
	struct dm_table *map;

	/*
	 * io objects are allocated from here.
	 */
	mempool_t *io_pool;
	mempool_t *tio_pool;

	struct bio_set *bs;

	/*
	 * Event handling.
	 */
	atomic_t event_nr;
	wait_queue_head_t eventq;
	atomic_t uevent_seq;
	struct list_head uevent_list;
	spinlock_t uevent_lock; /* Protect access to uevent_list */

	/*
	 * freeze/thaw support require holding onto a super block
	 */
	struct super_block *frozen_sb;
	struct block_device *bdev;

	/* forced geometry settings */
	struct hd_geometry geometry;

	/* sysfs handle */
	struct kobject kobj;

	/* zero-length flush that will be cloned and submitted to targets */
	struct bio flush_bio;
};

struct dm_table {
	struct mapped_device *md;
	atomic_t holders;
	unsigned type;

	/* btree table */
	unsigned int depth;
	unsigned int counts[MAX_DEPTH];	/* in nodes */
	sector_t *index[MAX_DEPTH];

	unsigned int num_targets;
	unsigned int num_allocated;
	sector_t *highs;
	struct dm_target *targets;

	struct target_type *immutable_target_type;
	unsigned integrity_supported:1;
	unsigned singleton:1;

	/*
	 * Indicates the rw permissions for the new logical
	 * device.  This should be a combination of FMODE_READ
	 * and FMODE_WRITE.
	 */
	fmode_t mode;

	/* a list of devices used by this table */
	struct list_head devices;

	/* events get handed up using this callback */
	void (*event_fn)(void *);
	void *event_context;

	struct dm_md_mempools *mempools;

	struct list_head target_callbacks;
};

/*
 * Linear: maps a linear range of a device.
 */
struct linear_c {
	struct dm_dev *dev;
	sector_t start;
  int len;
  char* bitset;
  char name[DM_NAME_LEN];
};

static int bitset_len(int sectors) {
    /* This works because the NULL-pointer expression is never
       executed -- the expression is evaluated to a constant at
       compile time */
  int bitset_type_size_byte = sizeof(*((struct linear_c*)0)->bitset);
  int size = sectors/bitset_type_size_byte;
  if (sectors % bitset_type_size_byte) size += 1;
  return size;
}

static int ct_init(const char* name);
static void ct_exit(const char* name);

/*
 * Construct a linear mapping: <dev_path> <offset>
 */
static int linear_ctr(struct dm_target *ti, unsigned int argc, char **argv)
{
	struct linear_c *lc;
	unsigned long long tmp;
	char dummy;

	if (argc != 2) {
		ti->error = "Invalid argument count";
		return -EINVAL;
	}

	lc = kmalloc(sizeof(*lc), GFP_KERNEL);
	if (lc == NULL) {
		ti->error = "dm-linear: Cannot allocate linear context";
		return -ENOMEM;
	}

	if (sscanf(argv[1], "%llu%c", &tmp, &dummy) != 1) {
		ti->error = "dm-linear: Invalid device sector";
		goto bad;
	}
	lc->start = tmp;

	if (dm_get_device(ti, argv[0], dm_table_get_mode(ti->table), &lc->dev)) {
		ti->error = "dm-linear: Device lookup failed";
		goto bad;
	}

  lc->len = ti->len;
  DMDEBUG("len=%llu\n",
          (unsigned long long)ti->len);
  DMDEBUG("lc->dev->name=%s\n",
          lc->dev->name);
  {
      char name[DM_NAME_LEN];
      /* struct mapped_device* md = dm_get_md(lc->dev->bdev->bd_dev); */
      dm_copy_name_and_uuid(ti->table->md, name, NULL);
      /* ti->table->md */
      DMDEBUG("dm-linear=%s\n",
              name);
      strncpy(lc->name, name, DM_NAME_LEN);
      ct_init(name);
  }

  {
      int size = bitset_len(ti->len);
      DMDEBUG("lc->bitset size= %d bytes", size);
      lc->bitset = kmalloc(size, GFP_KERNEL);
      memset(lc->bitset, 0, size);
  }

	ti->num_flush_requests = 1;
	ti->num_discard_requests = 1;
	ti->private = lc;
	return 0;

      bad:
	kfree(lc);
	return -EINVAL;
}

static void linear_dtr(struct dm_target *ti)
{
  char name[DM_NAME_LEN];
	struct linear_c *lc = (struct linear_c *) ti->private;
  int r;

  r = dm_copy_name_and_uuid(ti->table->md, name, NULL);
  if (r == -ENXIO) {
    DMDEBUG("dm_copy_and_uuid ENXIO");
  }
  DMDEBUG("linear_dtr() dm-linear=%s\n",
          name);

  ct_exit(lc->name);

  kfree(lc->bitset);
	dm_put_device(ti, lc->dev);
	kfree(lc);
}

static sector_t linear_map_sector(struct dm_target *ti, sector_t bi_sector)
{
	struct linear_c *lc = ti->private;

	return lc->start + dm_target_offset(ti, bi_sector);
}

static void linear_map_bio(struct dm_target *ti, struct bio *bio)
{
	struct linear_c *lc = ti->private;
  unsigned int index;
  unsigned int offset;
  int is_write = 0;
  int is_read = 0;

  /* TODO is this a write? */
	switch(bio_rw(bio)) {
	case READ:
    is_read = 1;
		break;
	case READA:
    is_read = 1;
    break;
	case WRITE:
    is_write = 1;
		break;
	}

  DMDEBUG("bi_sector=%llu bi_size=%u is_read=%d is_write=%d\n",
          (unsigned long long)bio->bi_sector,
          bio->bi_size,
          is_read,
          is_write);

  /* TODO set bit in bitset */
  if (is_write) {
      index = bio->bi_sector / sizeof(*(lc->bitset));
      offset = bio->bi_sector % sizeof(*(lc->bitset));
      lc->bitset[index] |= 1 << offset;
  }

	bio->bi_bdev = lc->dev->bdev;
	if (bio_sectors(bio))
		bio->bi_sector = linear_map_sector(ti, bio->bi_sector);
}

static int linear_map(struct dm_target *ti, struct bio *bio,
		      union map_info *map_context)
{
	linear_map_bio(ti, bio);

	return DM_MAPIO_REMAPPED;
}

static int linear_status(struct dm_target *ti, status_type_t type,
			 unsigned status_flags, char *result, unsigned maxlen)
{
	struct linear_c *lc = (struct linear_c *) ti->private;

	switch (type) {
	case STATUSTYPE_INFO:
		result[0] = '\0';
		break;

	case STATUSTYPE_TABLE:
		snprintf(result, maxlen, "%s %llu", lc->dev->name,
				(unsigned long long)lc->start);
		break;
	}
	return 0;
}

static int linear_ioctl(struct dm_target *ti, unsigned int cmd,
			unsigned long arg)
{
	struct linear_c *lc = (struct linear_c *) ti->private;
	struct dm_dev *dev = lc->dev;
	int r = 0;

	/*
	 * Only pass ioctls through if the device sizes match exactly.
	 */
	if (lc->start ||
	    ti->len != i_size_read(dev->bdev->bd_inode) >> SECTOR_SHIFT)
		r = scsi_verify_blk_ioctl(NULL, cmd);

	return r ? : __blkdev_driver_ioctl(dev->bdev, dev->mode, cmd, arg);
}

static int linear_merge(struct dm_target *ti, struct bvec_merge_data *bvm,
			struct bio_vec *biovec, int max_size)
{
	struct linear_c *lc = ti->private;
	struct request_queue *q = bdev_get_queue(lc->dev->bdev);

	if (!q->merge_bvec_fn)
		return max_size;

	bvm->bi_bdev = lc->dev->bdev;
	bvm->bi_sector = linear_map_sector(ti, bvm->bi_sector);

	return min(max_size, q->merge_bvec_fn(q, bvm, biovec));
}

static int linear_iterate_devices(struct dm_target *ti,
				  iterate_devices_callout_fn fn, void *data)
{
	struct linear_c *lc = ti->private;

	return fn(ti, lc->dev, lc->start, ti->len, data);
}

static struct target_type linear_target = {
	.name   = "linear",
	.version = {1, 1, 0},
	.module = THIS_MODULE,
	.ctr    = linear_ctr,
	.dtr    = linear_dtr,
	.map    = linear_map,
	.status = linear_status,
	.ioctl  = linear_ioctl,
	.merge  = linear_merge,
	.iterate_devices = linear_iterate_devices,
};

/* http://lwn.net/Articles/22359/ */

/*
 * The sequence iterator functions.  We simply use the count of the
 * next line as our internal position.
 */
static void *ct_seq_start(struct seq_file *s, loff_t *pos)
{
    struct linear_c *lc = (struct linear_c*)s->private;
    loff_t *spos;

    if (*pos >= lc->len) return NULL;

    spos = kmalloc(sizeof(loff_t), GFP_KERNEL);
    if (! spos)
        return NULL;
    *spos = *pos;
    return spos;
}

static void *ct_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
    loff_t *spos = (loff_t *) v;
    struct linear_c *lc = (struct linear_c*)s->private;

    *pos = ++(*spos);
    if (*spos >= lc->len) return NULL;
    else return spos;
}

static void ct_seq_stop(struct seq_file *s, void *v)
{
    DMDEBUG("ct_seq_stop enter");
    kfree (v);
    DMDEBUG("ct_seq_stop exit");
}

/*
 * The show function.
 */
static int ct_seq_show(struct seq_file *s, void *v)
{
    loff_t *spos = (loff_t *) v;
    struct linear_c *lc = (struct linear_c*)s->private;
    if (*spos < lc->len) {
        unsigned int index = *spos / sizeof(*lc->bitset);
        unsigned int offset = *spos % sizeof(*lc->bitset);
        int is_set = lc->bitset[index] & (1 << offset);
        if (is_set) {
          seq_printf(s, "%Ld\n", *spos);
        }
    } else {
        DMDEBUG("ASSERT: seq_file positions should never be larger than sector count");
    }
    return 0;
}

/*
 * Tie them all together into a set of seq_operations.
 */
static struct seq_operations ct_seq_ops = {
    .start = ct_seq_start,
    .next  = ct_seq_next,
    .stop  = ct_seq_stop,
    .show  = ct_seq_show
};


/*
 * Time to set up the file operations for our /proc file.  In this case,
 * all we need is an open function which sets up the sequence ops.
 */

static int ct_open(struct inode *inode, struct file *file)
{
    struct dm_ioctl param;
    struct mapped_device* md;
    struct seq_file *sf;
    struct linear_c *lc;
    int rv;

    DMDEBUG("name=%s\n",
            file->f_path.dentry->d_name.name);
    memset(&param, 0, sizeof(param));
    strncpy(param.name, file->f_path.dentry->d_name.name, DM_NAME_LEN);
    DMDEBUG("param.name %s", param.name);
    md = find_device(&param);
    if (md) {
        DMDEBUG("md %p", md);
        dm_put(md);
    }
    /* if (md) { */
    /*     struct linear_c *lc = (struct linear_c*)md->map->targets->private; */
    /*     int i = 0; */
    /*     for (i=0; i<sectors; i++) { */
            
    /*     } */
    /* } */
    
    rv = seq_open(file, &ct_seq_ops);

    if (rv != 0) return rv;
    sf = (struct seq_file*)file->private_data;
    lc = (struct linear_c*)md->map->targets->private;
    sf->private = lc;

    return rv;
};

/* Writing anything to the file clears the bitset. */
static ssize_t ct_write(struct file *file, const char __user *data, size_t len, loff_t *offset) {
    struct seq_file *sf = (struct seq_file*)file->private_data;
    struct linear_c *lc = (struct linear_c*)sf->private;

    memset(lc->bitset, 0, bitset_len(lc->len));

    return len;
}

/*
 * The file operations structure contains our open function along with
 * set of the canned seq_ ops.
 */
static struct file_operations ct_file_ops = {
    .owner   = THIS_MODULE,
    .open    = ct_open,
    .read    = seq_read,
    .llseek  = seq_lseek,
    .release = seq_release,
    .write   = ct_write
};


/*
 * Module setup and teardown.
 */

static int ct_init(const char *name)
{
    struct proc_dir_entry *entry;

    entry = create_proc_entry(name, 0, NULL);
    if (entry)
        entry->proc_fops = &ct_file_ops;
    return 0;
}

static void ct_exit(const char *name)
{
  DMDEBUG("ct_exit enter");
  remove_proc_entry(name, NULL);
  DMDEBUG("ct_exit exit");
}

int __init dm_linear_init(void)
{
	int r = dm_register_target(&linear_target);

	if (r < 0)
		DMERR("register failed %d", r);

	return r;
}

void dm_linear_exit(void)
{
	dm_unregister_target(&linear_target);
}
