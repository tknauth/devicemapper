#!/usr/bin/env python2.7

import os
import sys
import md5

block_size=4*1024

def main(dev_path) :
    fd = os.open(dev_path, os.O_RDONLY)
    block_nr = 0
    fstat = os.fstat(fd)

    assert (fstat.st_size % block_size) == 0

    for block_nr in range(fstat.st_size / block_size) :
        os.lseek(fd, block_nr*block_size, os.SEEK_SET)
        block = os.read(fd, block_size)
        md5hash = md5.new(block).digest()
        sys.stdout.write(md5hash)

    os.close(fd)

def print_usage() :
    print 'dmextract <block device/file>'

if __name__ == '__main__' :

    # If there is only one argument, we assume it is the device mapper
    # name. Otherwise, first parameter is the device, second parameter
    # is the proc file.

    if len(sys.argv) == 2 :
        dev_path = sys.argv[1]
    else :
        print_usage()
        sys.exit(1)

    main(dev_path)
