#!/bin/bash

# Create two block devices with identical content. Modify one block
# device and merge changes into the second block device. Merge process
# utilizes modified block information provided by custom device mapper
# module. After the merge process, the checksums on both devices must
# match again.

# set -x

# Create two identical files with random content
dd if=/dev/urandom of=/tmp/block001.img bs=1M count=10
dd if=/tmp/block001.img of=/tmp/block002.img

losetup /dev/loop0 /tmp/block001.img
losetup /dev/loop1 /tmp/block002.img

modprobe dm-mod
sectors=$(blockdev --getsz /dev/loop0)
devblocks=$((sectors/8))
echo 0 $sectors linear /dev/loop0 0 | dmsetup create block001

# Modify some blocks at random
for i in $(seq 0 10) ; do
    maxblock=$((devblocks-1))
    block=$(shuf -i 0-$maxblock -n 1)
    echo $block
    dd if=/dev/urandom of=/dev/mapper/block001 bs=4k count=1 seek=$block
done

# Compute checksum
checksum_block001=$(md5sum /dev/mapper/block001 | cut -d' ' -f1)

# Merge changes into second block device. Checksum must match after merge.
CODEDIR=$(cd $(dirname $0) ; pwd)
python $CODEDIR/dmextract.py block001 | python $CODEDIR/dmmerge.py /dev/loop1

checksum_loop1=$(md5sum /dev/loop1 | cut -d' ' -f1)

echo $checksum_loop1 $checksum_block001
if [ "$checksum_block001" = "$checksum_loop1" ] ; then
    echo MATCHING checksums
else
    echo checksums do not match
fi

# Tear down
dmsetup remove block001
losetup -d /dev/loop1
losetup -d /dev/loop0
rmmod dm-mod
