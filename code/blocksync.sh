#!/usr/bin/env bash

# set -x

# Block-wise synchronization of two files. Computes per-block md5 hash
# and transfers only mismatching blocks. Used as an alternative
# baseline to rsync. rsync seems to be terribly slow for synchronizing
# two files.

# Source file. Must be local.
SRC=$1
# Destination file. Must be remote. Syntax follows rsync/scp, i.e.,
# <remote ip>:<file>. For example, 192.168.2.2:/home/thomas/16GB.dat
DST=$2

DSTHOST=${2%%:*}
DSTFILE=${2##*:}

mkfifo src
mkfifo dst
mkfifo blocks

md5sum.py $SRC > src &
ssh $DSTHOST md5sum.py $DSTFILE > dst &
md5cmp.py src dst > blocks &

dmextract.py $SRC blocks | ssh $DSTHOST dmmerge.py $DSTFILE

rm src dst blocks
