#!/bin/bash

# set -x

# Update a certain percentage of blocks in a block device with random
# data.

DEV=$1
PERCENT=$2

SECTORS=$(blockdev --getsz $DEV)
BLOCKS=$((SECTORS/8))
UPDATE_BLOCKS=$((BLOCKS*PERCENT/100))

dd if=/dev/zero bs=4k count=$UPDATE_BLOCKS 2> /dev/null | \
        openssl enc -aes-256-cbc -pass pass:"$(dd if=/dev/urandom bs=128 count=1 2>/dev/null | base64)" -nosalt > /tmp/block-data

shuf -n $UPDATE_BLOCKS -i1-$((BLOCKS-1)) | sort -n > /tmp/blocks

~/devicemapper/code/dmmerge.py $DEV /tmp/blocks /tmp/block-data
