#!/usr/bin/env python2.7

import os
import sys
import md5

block_size=4*1024
hashsize_byte = 16

def main(hashfile_1, hashfile_2) :
    fds = [os.open(hashfile_1, os.O_RDONLY),
           os.open(hashfile_2, os.O_RDONLY)]

    i = 0
    while True :
        hashes = [os.read(fds[0], hashsize_byte),
                  os.read(fds[1], hashsize_byte)]

        if hashes[0] == '' :
            break
        elif hashes[0] != hashes[1] :
            print i

        
        i += 1

    os.close(fds[0])
    os.close(fds[1])

def print_usage() :
    print 'dmextract <md5 hash file> <md5 hash file'

if __name__ == '__main__' :

    # If there is only one argument, we assume it is the device mapper
    # name. Otherwise, first parameter is the device, second parameter
    # is the proc file.

    if len(sys.argv) == 3 :
        hashfile_1 = sys.argv[1]
        hashfile_2 = sys.argv[2]
    else :
        print_usage()
        sys.exit(1)

    main(hashfile_1, hashfile_2)
