#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/mman.h>

#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)

static const long PAGE_SIZE = 4 * 1024;

int main(int argc, char* argv[]) {

    int fd_dev = open(argv[1], O_RDONLY);
    int fd_blocks = open(argv[2], O_RDONLY);
    int fd_stdout = 1;
    int fd_stderr = 2;
    int ret;

    assert(fd_dev != 0);
    assert(fd_blocks != 0);

    FILE* f_blocks = fdopen(fd_blocks, "r");
    assert(f_blocks != 0);

#ifdef MMAP
    struct stat file_stat;
    ret = fstat(fd_dev, &file_stat);
    assert(ret == 0);
    
    char* m = (char*) mmap(NULL,
                           file_stat.st_size,
                           PROT_READ,
                           MAP_PRIVATE,
                           fd_dev,
                           0);
    if (MAP_FAILED == m) {
        handle_error("mmap");
    }
    assert(MAP_FAILED != m);
    ret = posix_madvise(m, file_stat.st_size,
                        POSIX_FADV_RANDOM | POSIX_FADV_NOREUSE);
    assert(0 == ret);
#endif

    long block_nr;
    long counter = 0;
    char buf[PAGE_SIZE];
    while (EOF != fscanf(f_blocks, "%ld", &block_nr)) {
        long offset = block_nr * PAGE_SIZE;
        off_t ret = lseek(fd_dev, offset, SEEK_SET);
        assert(ret != (off_t) -1);

        ssize_t bytes_read = read(fd_dev, buf, PAGE_SIZE);
        assert(bytes_read == PAGE_SIZE);

        counter++;
        if (counter % 1000 == 0) {
#ifdef MMAP
            posix_madvise(m, offset, POSIX_MADV_DONTNEED);
#endif
            /* posix_fadvise(fd_dev, 0, offset, POSIX_FADV_DONTNEED); */
        }
        

        /* write to standard out */
        printf("%ld\n", block_nr);
#ifdef MMAP
        write(fd_stdout, (void*) m[offset], PAGE_SIZE);
#endif
        write(fd_stdout, (void*) buf, PAGE_SIZE);
    }


    /* munmap automatically done at end of program */

    return 0;
}
